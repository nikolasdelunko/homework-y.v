import logo from './logo.svg';
import './App.css';
import React from 'react'

class App extends React.Component {
    state = {
        procent:50,
        step:5
    }

    render() {
        const {procent, step} = this.state
        return (
            <div className="App">
                <button onClick={this.increaseCounter}>ADD</button>
                <h1>{procent}%</h1>
                <button onClick={this.decreaseCounter}>DEL</button>
                <h2>- </h2>
                <button onClick={this.increaseStep}>+</button>
                <h2>{step}</h2>
                <button onClick={this.decreaseStep}>-</button>


            </div>
        );
    }
    decreaseCounter = () => {
        const {procent, step} = this.state
        if(procent - step < 0){
            this.setState({procent: 0})

        }else{
            this.setState({procent: procent - step})
        }
    }
    increaseCounter = () => {
        const {procent, step} = this.state
        if(procent + step  >= 100){
            this.setState({procent: 0})
        }else{
            this.setState({procent: procent + step})
        }
    }
    decreaseStep = () => {
        const {step} = this.state
        if(step > 1){
         this.setState({step: step - 1})
        }

    }
    increaseStep = () => {
        const {step} = this.state
        if(step < 10){
         this.setState({step: step + 1})
        }
    }
}

export default App;
