import React, {useEffect} from 'react';
import axios from "axios";
import Loader from "../Loader/Loader";
import MovieList from "../MovieList/MovieList";
import {connect} from "react-redux";

const Movies = ({movies, isLoading, setMovies}) => {

  useEffect(() => {
    axios("https://ajax.test-danit.com/api/swapi/films")
      .then(res => {
        setMovies(res.data);
      })
  }, [])

  return (
    <div className="App">
      {isLoading && <Loader/>}
      <MovieList movies={movies}/>
    </div>
  );
}

const mapStateToProps = (state) => {
  console.log(state)
  return {
    movies: state.films.data,
    isLoading: state.films.isLoading
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setMovies: (movies) => dispatch({type: 'SET_MOVIES', payload: movies})
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Movies);
