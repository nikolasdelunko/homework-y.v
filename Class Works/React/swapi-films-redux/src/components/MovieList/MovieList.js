import React, {Component} from 'react';
import Movie from "../Movie/Movie";

const MovieList = ({movies}) => {
  const movieList = movies.map(x => <Movie movie={x} key={x.id}/>)

  return (
    <div>
      <ol>{movieList}</ol>
    </div>
  );
}

export default MovieList;