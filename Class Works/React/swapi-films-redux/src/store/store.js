import {createStore} from "redux";

const initialState = {
  films : {
    data: [],
    isLoading: true,
    error: false
  },
  film : {
    data: null,
    isLoading: true,
    error: false
  }
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_MOVIES' : {
      return {...state, films: {...state.films, data: action.payload, isLoading: false}}
    }
    case 'SET_MOVIE' : {
      return {...state, film: {...state.film, data: action.payload, isLoading: false}}
    }
    default : {
      return state
    }
  }

}

const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )

export default store;