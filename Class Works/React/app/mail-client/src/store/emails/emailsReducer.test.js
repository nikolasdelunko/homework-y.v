import reducer from './reducer'
import types from "../email/types";

const state = {
    data: [],
    isLoading: true,
    error: null
}


describe('Emails reducer test', () => {
    test('SET_EMAILS sets data to data prop', () => {
            const testEmails = [{id: 1}, {id: 2}, {id: 3}]
        const action = {
            type: types.SET_EMAILS,
            payload: testEmails
        }
        const newState = reducer(state, action)
        expect(newState.data).toBe(testEmails)
})
})