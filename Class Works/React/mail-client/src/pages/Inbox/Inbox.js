import React, {Component} from 'react';
import Email from "../Email/Email";
import PropTypes from 'prop-types'

class Inbox extends Component {
  render() {
    const {emails} = this.props;

    const emailCards = emails.map(e => <Email key={e.id} email={e} />)

    return (
      <div>
        {/*<button onClick={incrementAge}>Increment age</button>*/}
        {/*<button onClick={updateTitle}>*/}
        {/*  Update title*/}
        {/*</button>*/}
        <ul>
          {emailCards}
        </ul>
      </div>
    );
  }
}
Inbox.propTypes = {
    emails: PropTypes.arrayOf(PropTypes.object).isRequired
}

export default Inbox;