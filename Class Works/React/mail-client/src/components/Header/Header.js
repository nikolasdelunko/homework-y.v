import React, {Component} from 'react';
import './Header.scss';

class Header extends Component {
  render() {
    const {title, user} = this.props;

    return (
      <div>
        <h3>Header</h3>
        <h2>{title}</h2>
        <h3>Some other text</h3>
        {!!user.name && <div>{user.name}</div>}
        <div>{user.age}</div>
      </div>
    )
  }
}

export default Header;