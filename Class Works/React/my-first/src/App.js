import React, {Component} from 'react';
// import './App.scss';
import Header from './components/Header/Header';
import Inbox from "./components/Inbox/Inbox";
import Footer from "./components/Footer/Footer";


const emails = [
    {id: 1, topic: 'Email 1 topic'},
    {id: 2, topic: 'Email 2 topic'},
    {id: 3, topic: 'Email 3 topic'}
]


class App extends React.Component {
    state = {
        title: 'Mail Client',
        user: {
            name: 'YV',
            age: 28,
            email: "kakalut@gmak.com",
            cart: []
        }
    }

    incrementAge = () => {
        const {user} = this.state
        this.setState({
            user: {
                ...user,
                age: user.age + 1
            }
        })
    }

    updateTitle = () => {
        this.setState({title: 'Updated title'})
    }

    render() {
        const {title, user} = this.state

        return (
            <div className="App">
                <Header title={title} user={user} />
                <Inbox incrementAge={this.incrementAge} updateTitle={this.updateTitle} />
                <Footer />
            </div>
        );
    }
}

export default App;
