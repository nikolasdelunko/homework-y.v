import React, {Component} from 'react';
// import './Email.scss';

class Email extends Component {
    render() {
        const {email} = this.props;

        return (
            <li className='email'>
                {email.topic}
            </li>
        );
    }
}

export default Email;
