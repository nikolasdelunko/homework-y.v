import React from 'react';
import {Route, Switch, Redirect} from "react-router-dom";
import Inbox from "../pages/Inbox/Inbox";
import Sent from "../pages/Sent/Sent";
import Favorites from "../pages/Favorites/Favorites";
import Error404 from "../pages/Error404/Error404";
import OneEmail from "../pages/OneEmail/OneEmail";
import Login from "../pages/Login/Login";
import ProtectedRoute from "./ProtectedRoute";
import {useSelector} from "react-redux";

const AppRoutes = () => {
  const user = useSelector(state => state.user.data)
  const isLoggedIn = !!user;

  return (
    <Switch>
      <Redirect exact from='/' to='/inbox' />
      {isLoggedIn && <Redirect exact from='/login' to='/' />}
      <ProtectedRoute exact path='/inbox'><Inbox /></ProtectedRoute>
      <ProtectedRoute exact path='/inbox/:emailId'><OneEmail /></ProtectedRoute>
      <ProtectedRoute exact path='/sent'><Sent /></ProtectedRoute>
      <ProtectedRoute exact path='/favorites'><Favorites /></ProtectedRoute>
      <Route exact path='/login'><Login /></Route>
      <Route path='*'><Error404 /></Route>
    </Switch>
  );
};

export default AppRoutes;