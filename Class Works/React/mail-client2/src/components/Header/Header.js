import React from 'react';
import './Header.scss';
import PropTypes from 'prop-types';
import {connect} from "react-redux";

const Header = ({title = "Default title", user}) => {
  return (
    <div>
      <h3>Header</h3>
      <h2>{title}</h2>
      {user && <h4>{user.login}</h4>}
    </div>
  )
}

Header.propTypes = {
  title: PropTypes.string
}

// Header.defaultProps = {
//   title: "Default title"
// }

const mapStateToProps = (state) => {
  return {
    user: state.user.data
  }
}

export default connect(mapStateToProps)(Header);