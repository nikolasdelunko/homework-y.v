import React from 'react';
import * as Icons from '../../theme/icons';

const Icon = ({type, className = '', ...rest}) => {
  const iconJsx = Icons[type]

  if (!iconJsx) {
    return null;
  }

  return (
    <span className={`icon icon--type ${className}`}>
      {iconJsx({...rest})}
    </span>
  );
};

export default Icon;