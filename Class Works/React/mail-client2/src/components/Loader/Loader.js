import React, {Component, useEffect} from 'react';
import './Loader.scss';
import axios from "axios";

const Loader = () => {
  useEffect(() => {
    console.log('Mounted loader')

    return () => {
      console.log('Unmounting Loader...')
    }
  }, []);

  return (
    <div className='loader-wrapper'>
      <div className="loader">Loading...</div>
    </div>
  );
}

export default Loader;