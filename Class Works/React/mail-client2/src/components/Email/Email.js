import React from 'react';
import './Email.scss';
import PropTypes from 'prop-types';
import Icon from "../Icon/Icon";
import {Link} from "react-router-dom";

// function Email () {
const Email = ({email, showFull}) => {
  return (
    <li className='email'>
      <div>
        <Link to={`/inbox/${email.id}`}>{email.topic}</Link>
        <Icon type='star' color='green'/>
      </div>
      {showFull && <div>{email.body}</div>}
    </li>
  );
}

// string, bool, func, object, array, symbol, number
// isRequired
// instanceOf, oneOf([1, 2, 3]), oneOfType([PropTypes.string, PropTypes.func]), arrayOf(PropTypes.number), shape(), exact()
Email.propTypes = {
  // email: PropTypes.object.isRequired,
  email: PropTypes.exact({
    id: PropTypes.number.isRequired,
    topic: PropTypes.string.isRequired,
    body: PropTypes.string
  }).isRequired,
}

export default Email;