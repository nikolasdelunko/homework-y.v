import React, {Component, useEffect, useState} from 'react';
import Email from "../../components/Email/Email";
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";
import Loader from "../../components/Loader/Loader";

const Inbox = () => {
  const emails = useSelector(state => state.emails.data)
  const isLoading = useSelector(state => state.emails.isLoading)
  const dispatch = useDispatch()

  useEffect(() => {
    axios('/api/emails')
      .then(res => {
        dispatch({type: 'SET_EMАILS', payload: res.data})
      })
  }, []);

  if (isLoading) {
    return <Loader/>
  }

  const emailCards = emails.map(e => <Email key={e.id} email={e}/>)

  return (
    <div>
      {emails.length === 0 && <div>You don't have any emails</div>}
      <ul>
        {emailCards}
      </ul>
    </div>
  );
}

// Inbox.propTypes = {
//   // emails: PropTypes.arrayOf(PropTypes.object).isRequired
//   emails: PropTypes.arrayOf(PropTypes.shape({
//     id: PropTypes.number.isRequired,
//     topic: PropTypes.string.isRequired
//   })).isRequired
// }

export default Inbox;