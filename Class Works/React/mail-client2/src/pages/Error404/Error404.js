import React, {Component} from 'react';
import './Error404.scss';

class Error404 extends Component {
  render() {
    return (
      <div className='error-404'>
        <h2>This page does not exist</h2>
        <h4>But we have cookies instead</h4>
        <div>
          <img src='https://www.swixsport.com/globalassets/banners/chocolatechipcookies.jpg' alt='Cookes'/>
        </div>
      </div>
    );
  }
}

export default Error404;