import React, {useRef, useState} from 'react';
import './Login.scss';
import axios from "axios";
import {connect} from "react-redux";

const Login = ({setUser}) => {
  const [loginError, setLoginError] = useState(null);
  const loginRef = useRef();
  const passwordRef = useRef();

  const handleSubmit = (e) => {
    e.preventDefault()
    const login = loginRef.current.value;
    const password = passwordRef.current.value;

    axios.post('/api/login', {login, password})
      .then(res => setUser(res.data))
      .catch(() => setLoginError("Login or password is incorrect"))
  }

  return (
    <form onSubmit={handleSubmit}>
      {loginError && <div className='error'>{loginError}</div>}
      <div>
        <input type='text' placeholder='Login' ref={loginRef} required/>
      </div>
      <div>
        <input type='password' placeholder='Password' ref={passwordRef} required/>
      </div>
      <div>
        <button type='submit'>Submit</button>
      </div>
    </form>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    setUser: (user) => dispatch({type: 'SET_USER', payload: user})
  }
}

export default connect(null, mapDispatchToProps)(Login);