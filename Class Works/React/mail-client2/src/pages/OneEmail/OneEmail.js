import React, {useEffect, useState} from 'react';
import {useParams, useHistory} from 'react-router-dom';
import axios from "axios";
import Loader from "../../components/Loader/Loader";
import Email from "../../components/Email/Email";

const OneEmail = () => {
  const [email, setEmail] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  const {emailId} = useParams();
  const history = useHistory();
  console.log('Rendering email')

  useEffect(() => {
    setIsLoading(true)
    axios(`/api/emails/${emailId}`)
      .then(res => {
        setEmail(res.data)
        setIsLoading(false)
      })
  }, [emailId])

  if (isLoading) {
    return <Loader />
  }

  return (
    <div>
      <Email email={email} showFull />
      <button onClick={() => history.push(`/inbox/${emailId - 1}`)}>Previous</button>
      <button onClick={() => history.push(`/inbox/${+emailId + 1}`)}>Next</button>
    </div>
  );
};

export default OneEmail;