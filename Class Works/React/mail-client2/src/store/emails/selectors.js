const getEmails = () => (state) => state.emails.data
const emailsLoading = () => (state) => state.emails.isLoading

export default {
  getEmails, emailsLoading
}