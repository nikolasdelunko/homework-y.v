import types from './types'

const setIsLoading = (isLoading) => ({
    type: types.IS_LOADING,
    payload: isLoading
})

const saveEmail = (email) => ({
    type: types.SAVE_EMAIL,
    payload: email
})

export default {
    setIsLoading, saveEmail
}