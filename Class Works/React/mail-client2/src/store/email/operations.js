import actions from './actions';
import axios from "axios";

const getEmail = (emailId) => (dispatch, getState) => {
    dispatch(actions.setIsLoading(true))
    axios(`/api/emails/${emailId}`)
        .then(res => {
            dispatch(actions.saveEmail(res.data))
            dispatch(actions.setIsLoading(false))
        })
}

export default {
    getEmail
}
