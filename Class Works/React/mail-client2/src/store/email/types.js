const IS_LOADING = 'mail-client/email/IS_LOADING'
const SAVE_EMAIL = 'mail-client/email/SAVE_EMAIL'


export default {
    IS_LOADING, SAVE_EMAIL
}