import {createStore} from "redux";

const initialState = {
  user: {
    data: null,
    isLoading: true,
    error: null
  },
  emails: {
    data: [],
    isLoading: true,
    error: null
  }
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_USER': {
      return {...state, user: {...state.user, data: action.payload, isLoading: false}}
    }
    case 'SET_EMАILS': {
      return {...state, emails: {...state.emails, data: action.payload, isLoading: false}}
    }
    default:
      return state;
  }
}

const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

export default store;