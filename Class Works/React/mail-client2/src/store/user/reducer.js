import  emailTypes from './types'



const initialState = {
        data: null,
        isLoading: true,
        error: null
    }

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case emailTypes.SAVE_EMAIL:{
            return {...state,  data: action.payload}
        }
        case emailTypes.IS_LOADING: {
            return {...state,  isLoading: action.payload}
        }

        default:
            return state;
    }
}

export default reducer

