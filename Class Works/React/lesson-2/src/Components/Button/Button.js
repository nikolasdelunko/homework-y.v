import React, {Component} from 'react';

class Button extends Component {
    render() {
        const {content, handleClick, disable} = this.props;

        return <button onClick={handleClick} disabled={disable}>{content}</button>;
    }
}

export default Button;
