import React, {Component} from 'react';
import Button from "../Button/Button";

class Numbers extends Component {
    render() {
        const {numbers, deleteNumber} = this.props;
        const generatedNumbers = numbers.map((n, index) => (
            <div key={index}>
                {n}<Button content='x' handleClick={() => deleteNumber(index)} />
            </div>
        ));

        return (
            <div>
                {generatedNumbers}
            </div>
        );
    }
}

export default Numbers;
