import React, {useEffect, useState} from 'react';
import axios from "axios";
import Loader from "../Loader/Loader";
import MovieList from "../MovieList/MovieList";

const Movies = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [movies, setMovies] = useState([]);

  useEffect(() => {
    axios("https://ajax.test-danit.com/api/swapi/films")
      .then(res => {
        setMovies(res.data);
        setIsLoading(false);
      })
  }, [])

  return (
    <div className="App">
      {isLoading && <Loader/>}
      <MovieList movies={movies}/>
    </div>
  );
}

export default Movies;
