import React, {Component} from 'react';
import Movies from "./Movie/Movie";

class MovieList extends Component {
    render() {
        const {movies} = this.props
        return (
            <div><ol>
                {movies.map(x => <Movies movie={x} key={x.id}/>)}
            </ol>

            </div>
        );
    }
}


export default MovieList;