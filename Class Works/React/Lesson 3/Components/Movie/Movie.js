import React, {Component, useState} from 'react';
import {useHistory} from 'react-router-dom';
import Characters from "../Characters/Characters";

const Movie = ({movie}) => {
  const history = useHistory();

  return (
    <li>
      <div>{movie.name}
        <button onClick={() => history.push(`/films/${movie.id}`)}>Детальнее</button>
      </div>
    </li>
  );
}

export default Movie;