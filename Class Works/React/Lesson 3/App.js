import './App.css';
import React, {useState} from 'react';
import AppRoutes from "./components/AppRoutes/AppRoutes";
import Header from "./components/Header/Header";

const App = () => {
    const [user, setUser] = useState()
    return (
        <div className="App">
            <Header user={user} setUser={setUser}/>
            <AppRoutes  user={user} setUser={setUser} />
        </div>
    );
}

export default App;
