import React from 'react';
import {Route, Redirect} from "react-router-dom";

const ProtectedRoute = ({isLogin, children, ...props}) => {
  return (
    <Route {...props}>
      {isLogin ? children :  <Redirect to={"/login"}/>}
    </Route>
  );
};

export default ProtectedRoute;