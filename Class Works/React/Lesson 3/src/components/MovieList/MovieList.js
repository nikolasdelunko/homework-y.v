import React, {Component} from 'react';
import Movie from "../Movie/Movie";

class MovieList extends Component {
  render() {
    const {movies} = this.props
    const movieList = movies.map(x => <Movie movie={x} key={x.id}/>)

    return (
      <div>
        <ol>{movieList}</ol>
      </div>
    );
  }
}

export default MovieList;