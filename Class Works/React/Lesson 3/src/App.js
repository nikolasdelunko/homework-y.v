import './App.css';
import React, {Component} from 'react';
import axios from "axios";
import Loader from "./components/Loader/Loader";
import MovieList from "./components/MovieList/MovieList";
import AppRoutes from "./AppRoutes/AppRoutes";


class App extends Component {
  state= {
    isLoading: true,
    films: []
  }
  componentDidMount() {
    axios('https://ajax.test-danit.com/api/swapi/films')
        .then(res => {
          this.setState({films: res.data, isLoading: false})
        })
  }

  render() {
    return (
        <div>
          {this.state.isLoading  && <Loader />}
          <MovieList movies={this.state.films} />
          <AppRoutes />
        </div>
    );
  }
}

export default App;
