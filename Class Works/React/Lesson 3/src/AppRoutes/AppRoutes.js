import React from 'react';
import {Switch, Route, Redirect} from "react-router-dom";
import Movies from "../components/Movie/Movie";
import MovieDetails from "../components/MovieList/MovieList";

const AppRoutes = () => {
    return (
        <Switch>
            <Redirect from={'/'} exact to={'/films'}/>
            <Route path={'/films'}><Movies/></Route>
            <Route path={'/dilms/:filmId'}><MovieDetails/></Route>
        </Switch>
    )
};

export default AppRoutes;