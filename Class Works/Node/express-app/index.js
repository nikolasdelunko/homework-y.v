const express = require('express')
const mongoose = require('mongoose')
const path = require('path');
const fs = require('fs');
const crypto = require("crypto");
const exphbs = require('express-handlebars');
const Product = require('./models/product')

const PORT = 3000
const app = express()

app.use(express.static('public'))
app.use(express.urlencoded({ extended: true }))

app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');
// add middleware for support URL-encoded bodies

app.use((req, res, next) => {
	// const now = Date.now();
	console.log(req.url);
	// console.log(now);
	next()
})

app.get('/', (req, res) => {
	res.send(`
    <h1>Homepage</h1>
    <a href="/catalog">Catalog</a>
    <script>console.log("hello page")</script>
    `)
})

app.get('/catalog', async (req, res) => {
	const products = await getProducts()
	res.render('products', { title: 'Catalog | Products', products })
})

app.get('/catalog/:productId', async (req, res) => {
	const product = await Product.findById(req.params.productId)
	res.render('product', { title: product.name, product: product.toJSON() })
})

app.get("/api/products", async (req, res) => {
	const products = await getProducts()
	res.json(products)
})

app.get("/add-product", (req, res) => {
	const form = fs.readFileSync(path.join(__dirname, "public", 'add-product.html'), 'utf8')
	return res.send(form)
})

app.post("/products", async (req, res) => {
	if (!req.body) return res.status(400).send({ massage: "Bad request" });

	const { name, price } = req.body

	const newProduct = { name, price };

	try {
		const product = await Product.create(newProduct)
		console.log(product);
		res.status(303)
		res.redirect("/catalog")
	} catch (err) {
		res.writeHead(500)
		res.end()
	}
})

app.use("*", (req, res) => {
	res.status(404)
	res.send('<h2>Page not found</h2><a href="/">Go to home</a>')
})

mongoose
	.connect('mongodb+srv://admin:Up3dUTNnkIWNmOmi@cluster0.6btdg.mongodb.net/ecom?retryWrites=true&w=majority')
	.then(() => console.log('Database was connected'))
	.catch(err => console.error(err))


app.listen(PORT, () => {
	console.log(`Server is running at port ${PORT}`);
})

async function getProducts() {
	const products = await Product.find().lean()
	return products
}