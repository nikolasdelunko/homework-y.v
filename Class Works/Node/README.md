# Nodejs basics
## Plan

1. Command line
2. Write into files
3. Get few sites contents and save it locally
4. Configure simple http server using just nodejs

### Intro to nodejs

### Globals

* __dirname
* __filename
* modules
* process

### Built-in modules
#### OS
- userInfo
- uptime
- release
- totalmem
- freemem

**Practice:**
    
Вивести інформацію про систему: 

{username} is working on {system type}. The machine has {number} cores. You are active {n} hours

#### PATH

- sep
- join
- basename
- resolve

#### FS
- readFileSync vs readFile
- writeFile vs writeFileSync

**Practice:**

Вивести контент csv файлу у вигляді таблиці в консоль
    
```
Subject,Grade,Student
JS,90,Rostyslav
HTML,100,Rostyslav
PHP,89,Rostyslav
Python,87,Rostyslav
```
    
Додатково: дати можливість записувати через конколь нові дані в файл
    

### HTTP
1. Make get request to get content from danit
    1. save data to the file
2. Create server
    1. send simple html
    2. http status codes
    3. create few pages (home, about, products, 404 error page)

**Practice:**        
Show csv file from previous task as a table on route `/grades`

Advanced: try to use hbs as a template