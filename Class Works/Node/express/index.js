const express = require('express')
const path = require('path');
const fs = require('fs');
const crypto = require("crypto");

const PORT = 3000
const app = express()

app.use(express.static('public'))
app.use(express.urlencoded({ extended: true }))
// add middleware for support URL-encoded bodies


app.get('/', (req, res) => {
	res.send(`
    <h1>Homepage</h1>
    <a href="/catalog">Catalog</a>
    <script>console.log("hello page")</script>
    `)
})

app.get('/catalog', (req, res) => {
	const products = getProducts()
	const list = products.map(({ id, name, price }) => `
    <tr>
      <td>${id}</td>
      <td>${name}</td>
      <td>${price}</td>
    </tr>
  `).join('')
	const headings = ['id', 'name', 'price']
		.map(heading => `<th>${heading}</th>`)
		.join('')

	res.send(`
    <table>
    <tr>${headings}</tr>
    ${list}
    </table>
    `)
})

app.get("/api/products", (req, res) => {
	const products = getProducts()
	res.json(products)
})

app.get("/add-product", (req, res) => {
	const form = fs.readFileSync(path.join(__dirname, "public", 'add-product.html'), 'utf8')
	return res.send(form)
})

app.post("/products", (req, res) => {
	if (!req.body) return res.status(400).send({ massage: "Bad request" });
	// if req doesn't hav body send err bad request

	const id = crypto.randomBytes(16).toString("hex");
	// creat id by default node.js module crypto

	const newProduct = { ...req.body, id: id };
	const products = getProducts()
	products.push(newProduct)


	try {
		fs.writeFileSync(path.join(__dirname, 'products.json'), JSON.stringify(products, null, 2))

		res.status(303)
		res.redirect("/catalog")
	} catch (err) {
		res.writeHead(500)
		res.end()
	}
})


app.use("*", (req, res) => {
	res.status(404)
	res.send('<h2>Page not found</h2><a href="/">Go to home</a>')
})


app.listen(PORT, () => {
	console.log(`Server is running at port ${PORT}`);
})

function getProducts() {
	const data = fs.readFileSync(path.join(__dirname, 'products.json'), 'utf8')
	return JSON.parse(data);
}