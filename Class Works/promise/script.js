/* ЗАДАЧА-01
* Написать функцию, которая выводит сообщение на экран.
*
* Аргументы:
* 1 - текст сообщения
* 2 - длительность задержки
*
* Возвращаемое значение: Promise, внутри котрого и вызывается отсрочка.
* */
// const showMassage = (text, delay) => new Promise((resolve, reject) => {
//     setTimeout(()=> resolve(text),delay)
// })
//
// showMassage('Tratatad SDDD', 200).then(alert)

/* ЗАДАЧА-02
 * Дополнить функционал предыдущей задачи.
 * Показать на странцие 2 поля для ввода и кнопку 'Ok'.
 * В первое бользователь должен ввести сообщение, в второе - отсрочку в миллисекундах, через сколько сообщение нужно показать.
 *
 * Принцип работа остается то же, но срабатывать функция показа сообщения должна по клику на кнопку 'Ok'.
 * */

// ////// constructor method //////////
// const showMassage = (text, delay) => new Promise((resolve, reject) => {
//     setTimeout(()=> resolve(text),delay)
// })
//
//
// showMassage('Tratatad SDDD', 200).then(alert)
//
//
// class ShowMessage {
//     constructor(parent) {
//         this.elements = {
//             message: document.createElement('input'),
//             delay: document.createElement('input'),
//             error: document.createElement('p'),
//             btn: document.createElement('button'),
//             parent
//         }
//     }
//
//     render() {
//         const {message, delay, btn, error, parent} = this.elements;
//
//         btn.innerText = 'OK';
//         message.placeholder = 'enter message';
//         delay.placeholder = 'enter delay';
//         delay.type = 'number';
//         error.style.color = 'red';
//
//         message.addEventListener('change', (e) => this.handleInputChange(e))
//         delay.addEventListener('change', (e) => this.handleInputChange(e))
//
//         btn.addEventListener('click', (e) => {
//             this.handleButtonClick(e)
//         });
//
//         parent.append(error, message, delay, btn);
//     }
//
//     handleInputChange({target}) {
//         const {error} = this.elements;
//         if (target.value !== '' && target.value !== 0 && Boolean(target.value)) {
//             error.textContent = ''
//         } else {
//             error.textContent = 'invalid data'
//         }
//     }
//
//     handleButtonClick(e) {
//         e.preventDefault();
//         const {error, message, delay} = this.elements;
//         this.showMessage(message.value, delay.value)
//             .then(alert)
//             .catch(e => {
//                 error.textContent = e
//             })
//     }
//
//     showMessage(text, delay) {
//         const promiseCB = (resolve, rejected) => {
//             if (!text.length || !delay) return rejected("invalid data");
//
//             setTimeout(() => resolve(text), delay)
//         };
//
//         return new Promise(promiseCB);
//     };
// }
//
// const myShowMessage = new ShowMessage(document.querySelector('.container'))
// myShowMessage.render();


/////вариант топрный ///////


// const massage = document.createElement('input')
// const daley  = document.createElement('input')
// const btn = document.createElement('button')
// document.querySelector('.container').append(massage, daley, btn)
// btn.innerText = 'OK'
// massage.placeholder = 'type massage'
// daley.placeholder = 'enter daley'
//
// btn.addEventListener("click", () => {
//     showMassage(massage.value, daley.value).then(alert)
// })


/* ЗАДАЧА-03
* Написать функцию makeCircle, которая создает и размещает на странице круг.
* Аргументы:
* 1 - диаметр круга, числом без приставки px
* 2 - цвет круга
* 3 - CSS класс, который нужно кругу присвоить
* 4 - время, по истечению которого нужно выполнить следующее дейтсиве
*
* Возвращаемое знаениче: Promise.
*
* Решить задачу нужно так, чтобы после вызова функции makeCircle, можно было вызвать then() и фнукция внутри него сработала через указанное время.
* */


class MakeCircle {
    constructor(parent, d, color, delay) {
        this.parent = parent;
        this.circle = document.createElement('div');
        this.d = d;
        this.color = color;
        this.delay = delay;
    }

    render(className) {
        this.circle.style.width = `${this.d}px`;
        this.circle.style.height = `${this.d}px`;
        this.circle.style.backgroundColor = this.color;

        this.circle.classList.add(className);

        return new Promise((res, rej) => this.handlePromise(res, rej));
    }

    handlePromise(resolve, rejected) {
        setTimeout(() => {
            this.parent.append(this.circle);
            resolve(this.circle)
        }, this.delay)
    }
}

const myCircle = new MakeCircle(
    document.querySelector('.container'),
    257,
    '#f5f5f5',
    2458
);

myCircle.render('gogi-circle')
    .then(r => {
        console.log(r);
    });

// function makeCircle(parent, d, color, className, delay) {
//     const circleElement = document.createElement('div');
//     circleElement.style.width = `${d}px`;
//     circleElement.style.height = `${d}px`;
//     circleElement.style.backgroundColor = color;
//
//     circleElement.classList.add(className);
//
//     return new Promise((resolve) => {
//         setTimeout(() => {
//             parent.append(circleElement);
//             resolve(circleElement)
//         }, delay)
//     })
// }
//
// makeCircle(
//     document.querySelector('.container'),
//     257,
//     '#f5f5f5',
//     'gogi-circle',
//     2458
// ).then((r) => {
//     console.log('circle successfully rendered', r);
// });
