// let request = new XMLHttpRequest()
// request.open('GET', 'items.json')
// request.send()
//
// request.onload = function () {
//     console.log(`Загруженно ${xml.status}, ${xml.response}`)
// }
//
//
// function myFetch({
//                      method = "GET",
//                      URL,
//                      headers,
//                      body
//                  }) {
//     const handlePromise = (makeResolve, makeReject) => {
//         let xml = new XMLHttpRequest();
//
//         xml.open("GET", "dist/items.json");
//         xml.send();
//
//         xml.onload = function () {
//             console.log(`Загружено: ${xml.status} ${xml.response}`);
//         };
//     };
//
//     return new Promise(handlePromise);
// }
//

////////////  task 1

class User {
    constructor({name, userName, email, phone, website}) {
        this.name = name
        this.userName = userName
        this.email = email
        this.phone = phone
        this.website = website
        this.elements = {
            wrapper: document.createElement('div'),
            button: document.createElement('button')
        }
    }
    render (parent) {
        const {wrapper, button} = this.elements
        wrapper.insertAdjacentHTML('afterbegin', `
<p>${this.name}</p>
<p>${this.userName}</p>
<p>${this.email}</p>
<p>${this.phone}</p>
<p>${this.website}</p>`)
        button.innerText = 'See posts'
        wrapper.append(button)
        parent.append(wrapper)
    }
}

fetch('https://ajax.test-danit.com/api/json/users').then(res => res.json()).then(usersData => {
    usersData.forEach((user) => {
        new User(user).render(document.querySelector('.container'))
    })
})


//////////////////////////////task 2

class UserForm {
    constructor() {
        this.elements = {
            form: document.createElement('form'),
            submitBtn: document.createElement('button')
        }
    }

    render(parent) {
        const {form, submitBtn} = this.elements;

        form.insertAdjacentHTML('afterbegin', `
    <input type="text" name="name" placeholder="Gogi"/>
    <input type="text" name="fullName" placeholder="full name, separated by space"/>
    <input type="email" name="email" placeholder="gogi@go.com"/>
    `);

        submitBtn.innerText = 'ok';
        submitBtn.addEventListener('click', (e) => this.handleSubmit(e));

        form.append(submitBtn);
        parent.append(form);
    }

    handleSubmit(e) {
        e.preventDefault();
        const userFormData = new FormData(this.elements.form);

        fetch('https://ajax.test-danit.com/api/json/users/',
            {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    name: userFormData.get('name'),
                    username: userFormData.get('fullName'),
                    email: userFormData.get('email')
                })
            }).then(res => res.json()).then(r => {
            new User(r).render(document.querySelector('.container'));
            this.elements.form.remove();
        });
    }
}

const newUserForm = new UserForm();
newUserForm.render(document.body);





