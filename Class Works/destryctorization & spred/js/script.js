// --------------------- task 1 -----------------

// const user = {
//     name: "Gogi",
//     lastName: "Gogovitch",
//     dob: '31.12.2000',
//     info: {
//         hobbie: 'eat',
//         namePet: "gogik"
//     }
//
// }
//
// const {name: Imya, lastName: Familiya, dob: br, info} = user
//
// console.log(`Hello ${Imya}`)

///////////-------------------  task  2 ---------------------
//
//
// const instaLeeds = [
//     {name: "user-1", age: 13},
//     {name: "user-2", age: 33},
//     {name: "user-3", age: 40},
//     {name: "user-4", age: 20},
// ];
// const fbLeeds = [
//     {name: "user-01", age: 8},
//     {name: "user-02", age: 80},
//     {name: "user-03", age: 64},
//     {name: "user-04", age: 3},
// ];
// const tokTokLeeds = [
//     {name: "user-001", age: 8},
//     {name: "user-002", age: 8},
//     {name: "user-003", age: 4},
//     {name: "user-004", age: 3},
// ];
//
// instaLeeds.forEach( function (user) {
//     user.adds='instagram'
// })
//
// fbLeeds.forEach( function (user) {
//     user.adds='facebook'
// })
//
// tokTokLeeds.forEach( function (user) {
//     user.adds='TikTok'
// })
//
//
// const clickLeads = [...instaLeeds, ...fbLeeds, ...tokTokLeeds]
//
// console.log(clickLeads)
//
// clickLeads.forEach( function (user) {
//     let  {name, adds} = user
//     const userInfo = document.createElement('p')
//     userInfo.textContent = `your name ${name}, and yor form ${adds}`
//     document.body.prepend(userInfo)
// })


////////----------- task 3 -------------
//
// const instaLeeds = [
//     {name: "user-1", age: 13},
//     {name: "user-2", age: 33},
//     {name: "user-3", age: 40},
//     {name: "user-4", age: 20},
// ];
// const fbLeeds = [
//     {name: "user-01", age: 8},
//     {name: "user-02", age: 80},
//     {name: "user-03", age: 64},
//     {name: "user-04", age: 3},
// ];
// const tokTokLeeds = [
//     {name: "user-001", age: 8},
//     {name: "user-002", age: 8},
//     {name: "user-003", age: 4},
//     {name: "user-004", age: 3},
// ];
//
// const clickedLeads = [
//     ...instaLeeds.map(u => ({...u, adds: 'instagram'})),
//     ...fbLeeds.map(u => ({...u, adds: 'facebook'})),
//     ...tokTokLeeds.map(u => ({...u, adds: 'tik-tok'}))
// ];
//
// function showWinners(srcArr, amount) {
//     const losers = srcArr.slice(amount);
//
//     const root = document.getElementById('root');
//     root.insertAdjacentHTML('beforeend', '<h2>WINNERS</h2>');
//
//     for (let i = 0; i < amount; i++) {
//         const {name, adds} = srcArr[i];
//         root.insertAdjacentHTML('beforeend', `<p>${name} - ${adds}</p>`)
//     }
//     root.insertAdjacentHTML('beforeend', '<h2>LOSERS</h2>');
//     losers.forEach(looser => {
//         const {name, adds} = looser;
//         root.insertAdjacentHTML('beforeend', `<p>${name} - ${adds}</p>`);
//     })
// // }
// //
// showWinners(clickedLeads, 5);
//

/* ЗАДАЧА - 4
* Написать функцию, возвращаемым значением которой является объект простой кофемашины.
*
* Это задание для команд по 4 человека
*
* Функционал кофемашины:
* 1 - хранит запасы продуктов в свойствах - milk, sugar, water, coffee. запасы перевычисляются каждый раз после выдачи напитка.
* 2 - содержит метод getDrinkPrice(drinkName), где аргумнет drinkName содержит имя напитка, стоимость которого должен вернуть метод.
* 3 - содержит метод recalculateSupplies(drink), где аргумент drink содержит в себе объект напитка с "рецептом" его приготовления
* 4 - render метод, который появляет кофемашину на экране. тут должны происходить создание всех элементов, котрые появятся на странице
*
* на странице должны отображаться поле для ввода и кнопка ОК.
* после того как пользователь ввел имя напитка - нужно найти нужный напиток в списке рецептов кофемашины, понять хватит ли припасов.
* если припасов не хватит для пригоотовление - уведомить пользователя.
* если припасов хватат - через 2 секунды вывести на экран пользователю `Here is your drink ${drinkName}`.
* если такого напитка нет - показать сообщение о некорректном вводе и очистить инпут от значения.
*/


class CoffeeMachine {
    drinks = [];
    elements = {
        form: document.createElement('form'),
        select: document.createElement('select'),
    };

    constructor(supplies, name, parent) {
        this.supplies = supplies;
        this.name = name;
        this.elements.parent = parent
    }

    render() {
        this.elements.select.insertAdjacentHTML('afterbegin', "<option value='' selected disabled>--choose you drink--</option>");

        const optionsHTML = this.drinks.map(({name: drinkName}) => `<option value='${drinkName}'>${drinkName}</option>`).join('');
        this.elements.select.insertAdjacentHTML('beforeend', optionsHTML);

        // this.elements.select.addEventListener('change', this.handleSelection.bind(this));
        this.elements.select.addEventListener('change', (e) => this.handleSelection(e));

        this.elements.form.append(this.elements.select);
        this.elements.parent.append(this.elements.form)
    };

    handleSelection(event) {
        this.selectedDrink = event.target.value
        console.log(this)
    };

    createDrink(name, drinkSupplies) {
        const newDrink = new Recipe(name, drinkSupplies);
        this.drinks.push(newDrink)
    };

    getDrinkPrice (drinkName) {
        const
    }

}


const myCF = new CoffeeMachine([
    new Supply('water', 5),
    new Supply('milk', 5),
    new Supply('tonic', 5),
    new Supply('coffee', 10),
    new Supply('sugar', 3)
], 'goodMachine', document.getElementById('root') )

myCF.createDrink('ekspezco', [
    new Supply('water', 0.1),
    new Supply('coffee', 0.01)
])

myCF.createDrink('espresso-tonic', [
    new Supply('ice', 0.1),
    new Supply('water', 0.1),
    new Supply('coffee', 0.01),
    new Supply('milk', 0.5)
])

const [name, ] = CoffeeMachine

recalculateSupplies (drink) {

}
console.log(myCF)
myCF.render()

