// task 1

const wrapper = document.querySelector('.wrapper')

wrapper.addEventListener('click',(ev) => {
    if (ev.target.classList.contains('show-text-btn')) {
        const textElement = document.querySelector('.text')
        textElement.innerText = ev.target.innerText
    }
})

// task 2

const showModalBtn = document.createElement('button')
showModalBtn.innerText = 'Show Modal'

showModalBtn.addEventListener('click', () => {
    const modalWrapper = document.querySelector('.modal-wrapper')
    modalWrapper.style.display = 'flex'

    modalWrapper.addEventListener('click', (ev) => {
        if (ev.target === ev.currentTarget || ev.target.classList.contains('modal-close')) {
            modalWrapper.style.display = 'none';
        }
    })
})

// task 3

// function createMadal () {
//     const mow
// }



document.body.prepend(showModalBtn)
