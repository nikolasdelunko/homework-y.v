/* ЗАДАЧА 1
* "Своровать" метод forEach у массива
* Дан массив елементов, точнее не массив а HTMLCollection из элементов списка
* Наша задача перебрать коллекцию и каждому елементу изменить текстовое содержимое.
* При этом нужно использовать именно метод forEach массива, не используя Array.from() или деструктуризацию.
*/


classList = document.getElementsByClassName('list-item')
let arr = []
arr.forEach.call(classList, elem => {
    elem.innerText = 'BestOfTheBest'
})
console.log(classList)

/* ЗАДАЧА - 2
* Создать объект пользователя со свойствами имени и возраста.
* Добавить к этому объекту метод getLogin() написанный при помощи стрелочной функции
* внутри метода обращаться к свойствам объекта только через this
* Заставить этот метод работать.
*/


function User(name, age) {
    this.name = name
    this.age = age
    this.getLogin = () => this.name + this.age
}

const user = new User('gogi', '50')
console.log(user.getLogin())

/* ЗАДАЧА 3
* Создать гибкую логин форму при помощи функции конструктора
* Аргументы:
*  1 - CSS селектор для поиска родительского элемента для формы
*  2 - объект с настройками для формы. пока что будет содержать только две настройки:
*       - функция валидации логина
*       - функция валидации пароля
*
* Функицонал
*  создаваться форма должна следующим образом
*  new LoginForm('.gogi', {
*   validateLogin: (value) => value,
*   validatePass: (value) => value
*  })
*
* После выполнения этой строки кода на странице внутри указанного селектором элемента должна появится форма
* внутри формы - текстовый инпут для логина, инпут с типом пароль, кнопка сабмит.
* При изменении текстового инпута должна работать функция валидаци логина,
* при изменении инпута с паролем должна работать функция валидаци пароля.
* При непрохождении валидации над инпутом должо показываться сообщение "not valid"
*/

function LoginForm(selector, options) {
    this.elements = {
        parent: document.querySelector(selector),
        form: document.createElement('form'),
        name: document.createElement('input'),
        login: document.createElement('input'),
        password: document.createElement('input'),
        submit: document.createElement('button')
    }
    this.isValid = false
    this.handleSubmit = (event) => {
        event.preventDefault()
        let isValidLogin = options.valideteLogin(this.elements.login.value)
        let isValidPassword = options.validetePass(this.elements.password.value)
        if (!isValidLogin || !isValidPassword) {
            alert('wrong DATA')
        }else {alert(`Welcome  ${this.elements.name.value.toUpperCase()}`)}
    }
    this.render = () => {
        this.elements.name.placeholder = 'Name'
        this.elements.login.placeholder = 'Login'
        this.elements.password.placeholder = 'Password'
        this.elements.password.type = 'password'
        this.elements.submit.innerText = 'Login'
        this.elements.form.addEventListener('submit', this.handleSubmit)
        this.elements.form.append(this.elements.name, this.elements.login, this.elements.password, this.elements.submit)
        this.elements.parent.append(this.elements.form)
    }
}

let myForm = new LoginForm('.form-wrapper', {
    valideteLogin : val => val.length > 5,
    validetePass : val  =>  val.length > 8,
})
myForm.render()





