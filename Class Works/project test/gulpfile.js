// let sass = require('gulp-sass')
// let gulp = require('gulp')
// const concat = require('gulp-concat')

import gulp from 'gulp'
import concat from 'gulp-concat'
import clean from 'gulp-clean'
import imagemin from 'gulp-imagemin'
// const browserSync = require('browser-sync').create()
import browser from 'browser-sync'
let browserSync = browser.create()


const path = {
    src: {
        // html: "src/index.html",
        css: 'src/**/*.css',
        js: 'src/**/*.js',
        img: 'src/img/**/*'
    },
    build: {
        // html: 'build/',
        css: 'build/css/',
        js: 'build/js/',
        img: 'build/img/'
    }
};



let buildStyles = () => (
    gulp.src(path.src.css)
        .pipe(concat('style.css'))
        .pipe(gulp.dest(path.build.css))
        .pipe(browserSync.stream())
)

let buildJS = () => (
    gulp.src(path.src.js)
        .pipe(concat('script.js'))
        .pipe(gulp.dest(path.build.css))
        .pipe(browserSync.stream())
)

// let buildHTML = () => (
//     gulp.src(path.src.html)
//         .pipe(gulp.dest(path.build.html))
//
// )

let cleanBuild = () => (
    gulp.src('build/', {allowEmpty: true})
        .pipe(clean())
)

let buildIMG = () => (
    gulp.src(path.src.img)
        .pipe(imagemin())
        .pipe(gulp.dest(path.build.img))
        .pipe(browserSync.stream())
)

const watcher = () => {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    })
    gulp.watch(path.src.css, buildStyles).on('change', browserSync.reload)
    gulp.watch(path.src.css, buildJS).on('change', browserSync.reload)
    gulp.watch(path.src.css, buildIMG).on('change', browserSync.reload)
    gulp.watch('./index.html',null).on('change', browserSync.reload)
}

gulp.task('build', gulp.series(
    cleanBuild,
    gulp.parallel([buildIMG, buildJS, buildStyles]),
    watcher
))


// gulp.task('buildHTML', function () {
//     gulp.src('src/index.html')
//         .pipe(gulp.dest('build/'))
//     }
// )
//
// gulp.task('default', function f() {
//     console.log(10)
// })
//
// gulp.task('hello', function () {
//     console.log('Hello Yaroslav')
// })
// //
// //     [crayon-60a82c78685be235747641 inline="true" ]
// // gulp.task('sass', function(){
// //     gulp.src('source-files')
// //         .pipe(sass()) // Using gulp-sass
// //         .pipe(gulp.dest('destination'))
// // });