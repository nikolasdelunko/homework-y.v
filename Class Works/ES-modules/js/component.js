class Component {
    constructor({parent, self , ...restEl}) {
        this.parant = parent
        this.self = self
        Object.entries(restEl).forEach((entry)=> {
            let [name, value] = entry
            this[name] = value
        })
    }
    render() {
        this.parant.append(this.self)
    }
}

export default Component
