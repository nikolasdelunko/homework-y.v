function Recipe(name, products) {
    this.name = name
    this.products = products
}

Recipe.prototype.noSugar = function () {
    const sugar = this.products.find(el => el.name.toLowerCase() === 'sugar')
    if (sugar) {
        sugar.quantity = 0
    }
}
Recipe.prototype.dobleSugar = function () {
    const sugar = this.products.find(el => el.name.toLowerCase() === 'sugar')
    if (sugar) {
        sugar.quantity *= 2
    }
}

function Supply(name, quantity) {
    this.name = name
    this.quantity = quantity
}

// console.log(new Supply('milk', '10L'))

