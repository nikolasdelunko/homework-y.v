const myCF = new CoffeeMachine([
    new Supply('water', 5),
    new Supply('milk', 5),
    new Supply('tonic', 5),
    new Supply('coffee', 10),
    new Supply('sugar', 3)
], 'goodMachine', document.getElementById('root') )

myCF.createDrink('ekspezco', [
    new Supply('water', 0.1),
    new Supply('coffee', 0.01)
])

myCF.createDrink('espresso-tonic', [
    new Supply('ice', 0.1),
    new Supply('water', 0.1),
    new Supply('coffee', 0.01),
    new Supply('milk', 0.5)
])

console.log(myCF)
myCF.render()
