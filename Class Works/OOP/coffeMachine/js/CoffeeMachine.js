class CoffeeMachine {
    drinks = [];
    elements = {
        form: document.createElement('form'),
        select: document.createElement('select'),
    };

    constructor(supplies, name, parent) {
        this.supplies = supplies;
        this.name = name;
        this.elements.parent = parent
    }

    render() {
        this.elements.select.insertAdjacentHTML('afterbegin', "<option value='' selected disabled>--choose you drink--</option>");

        const optionsHTML = this.drinks.map(drink => `<option value='${drink.name}'>${drink.name}</option>`).join('');
        this.elements.select.insertAdjacentHTML('beforeend', optionsHTML);

        // this.elements.select.addEventListener('change', this.handleSelection.bind(this));
        this.elements.select.addEventListener('change', (e) => this.handleSelection(e));

        this.elements.form.append(this.elements.select);
        this.elements.parent.append(this.elements.form)
    };

    handleSelection(event) {
        this.selectedDrink = event.target.value
        console.log(this)
    };

    createDrink(name, drinkSupplies) {
        const newDrink = new Recipe(name, drinkSupplies);
        this.drinks.push(newDrink)
    };
}

/////////////////////// прототипы //////////////////


// function CoffeeMachine(supplies, name, parent) {
//     this.drinks = []
//     this.supplies = supplies
//     this.name = name
//     this.elements = {
//         parent,
//         form: document.createElement('form'),
//         select: document.createElement('select'),
//     }
// }
//
// CoffeeMachine.prototype.render = function () {
//     this.elements.select.insertAdjacentHTML('afterbegin', "<option value='' selected disabled>--choose you drink--</option>");
//
//     const optionsHTML = this.drinks.map(drink => `<option value='${drink.name}'>${drink.name}</option>`).join('');
//     this.elements.select.insertAdjacentHTML('beforeend', optionsHTML);
//
//     // this.elements.select.addEventListener('change', this.handleSelection.bind(this));
//     this.elements.select.addEventListener('change', (e) => this.handleSelection(e));
//
//     this.elements.form.append(this.elements.select);
//     this.elements.parent.append(this.elements.form)
// };
//
// CoffeeMachine.prototype.handleSelection = function (event) {
//     this.selectedDrink = event.target.value
//     console.log(this)
// };
//
// CoffeeMachine.prototype.createDrink = function (name, drinkSupplies) {
//     const newDrink = new Recipe(name, drinkSupplies);
//     this.drinks.push(newDrink)
// };
//
