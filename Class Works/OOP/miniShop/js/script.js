class Product {
    constructor(title, price, description, category, availableAmount, sellerID) {
        this.ID = date.now()
        this.title = title
        this.price = price
        this.description = description
        this.category = category
        this.availableAmount = availableAmount
        this.sellerID = sellerID
    }
}

class Cart {
    constructor(userID, items) {
        this.userID = userID
        this.cost = this
        this.items = items
    }
    calcCost() {
        let summ = 0
        this.items.forEach(item => {
            summ += item.price
        })
        return summ
    }
    addItem(item) {
        this.items.push(item)
    }
    removeItem(itemID) {
        let index = this.items.findIndex(elem => elem.ID === itemID)
        if(index >= 0){
            this.items.splice(index,1)
        }
    }
    editAmount(itemID, newAmount) {}
    buy () {}

}

let USIN_ID_COUNTER = 0
class User {
    constructor(fullName, age, email, balance) {
        this.ID = ++USIN_ID_COUNTER
        this.fullName = fullName
        this.age = age
        this.email = email
        this.balance = balance
        this.cart = []

    }
    makePayment() {}
}