function sum(arr) {
    return arr.reduce(function (a, b) {
        return a + b
    })
}

module.exports.sum = sum