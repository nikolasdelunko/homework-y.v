const express = require('express')

let app = express()

const PORT = 8000

app.get('/', (req, res) => {
    res.send(`
<h1>Hello World</h1>
<a href="/catalog">Catalog</a>
<script>console.log("hello page")</script>
`)
})

app.get('/about', (req, res) => {
    res.send(`<h2>About</h2>`)
})

app.get('/catalog', (req, res) => {
    res.send(`<h2>Call us</h2>`)
})


app.listen(PORT, () => {
    console.log(`server start on port ${PORT}`)
})
