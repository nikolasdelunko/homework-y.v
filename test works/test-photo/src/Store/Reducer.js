import {combineReducers} from "redux";
import Products from './products'
import modal from './modal'


const reducer = combineReducers({
    Products,
    modal
})


export default reducer