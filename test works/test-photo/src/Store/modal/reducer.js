const initialState = {
    modal: false,
    currentId: [],
    currentUrl: [],
}


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_MODAL' : {
            return {...state, modal: action.payload}
        }
        case 'SET_CURRENT_ID' : {
            return {...state, currentId: action.payload}
        }

        case 'SET_CURRENT_URL' : {
            return {...state, currentUrl: action.payload}
        }
        default:
            return state
    }
}

export default reducer