import './App.css';
import React, {useEffect} from "react";
import axios from "axios";
import {useDispatch} from "react-redux";
import Tabs from './Components/tabs/Tabs'



function App() {
    const dispatch = useDispatch()

    useEffect(() => {
        axios.get('http://jsonplaceholder.typicode.com/photos')
            .then(res => {
                dispatch({type: 'SET_PRODUCTS', payload: res.data})
            })
        if (!localStorage.getItem("items")) localStorage.setItem("items", "[]");
    }, [])



    return (
        <div className="App">
            <div>
            <Tabs/>
            </div>
        </div>
    );
}




export default App

