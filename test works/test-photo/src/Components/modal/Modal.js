import './Modal.scss'
import React from 'react';

const Modal = (props) => {
    const {onClick, header, url, actions} = props;
    const handleCloseClick = (e) => {
        if (
            e.target.getAttribute("class") === "modal" ||
            e.target.getAttribute("class") === "modalContent__modalClose"
        ) {
            onClick();
        }
    };

    return (
        <div className="modal" onClick={handleCloseClick}>
            <div className="modal__container">
                <div className="modal__header">
                    <span className='modal__header-title'>{header}</span>
                    <span className="modal__header-close" onClick={onClick}/>
                </div>
                <div className="modal__content">
                    <img src={url} alt='big'/>
                    <div className='modal__control'>
                        {actions}
                    </div>

                </div>
            </div>
        </div>
    );
};

export default Modal;

