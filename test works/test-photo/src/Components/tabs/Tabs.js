import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import {useSelector} from "react-redux";
import Items from "../items/Items";

function TabPanel(props) {
    const {children, value, index, ...other} = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{p: 3}}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export default function BasicTabs() {
    const [value, setValue] = React.useState(0);
    const items = useSelector(state => state.Products.data)

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };


    let itemOne = items.filter(item => item.albumId === 1).map((item) => (
        <Items
            card={item}
        />
    ))


    let itemsTwo = items.filter(item => item.albumId === 2).map((item) => (
        <Items
            card={item}
        />
    ))
    let itemsThree = items.filter(item => item.albumId === 3).map((item) => (
        <Items
            card={item}
        />
    ))
    let itemsFour = items.filter(item => item.albumId === 4).map((item) => (
        <Items
            card={item}
        />
    ))
    let itemsFive = items.filter(item => item.albumId === 5).map((item) => (
        <Items
            card={item}
        />
    ))
    let itemsSix = items.filter(item => item.albumId === 6).map((item) => (
        <Items
            card={item}
        />
    ))
    let itemsSeven = items.filter(item => item.albumId === 7).map((item) => (
        <Items
            card={item}
        />
    ))
    let itemsEight = items.filter(item => item.albumId === 8).map((item) => (
        <Items
            card={item}
        />
    ))
    let itemsNine = items.filter(item => item.albumId === 9).map((item) => (
        <Items
            card={item}
        />
    ))
    let itemsTen = items.filter(item => item.albumId === 10).map((item) => (
        <Items
            card={item}
        />
    ))
    //
    // items.slice([450], [500]).map

    return (
        <Box sx={{width: '100%'}}>
            <Box className='search' sx={{borderBottom: 1, borderColor: 'divider'}}>
                <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                    <Tab label="1" {...a11yProps(0)} />
                    <Tab label="2" {...a11yProps(1)} />
                    <Tab label="3" {...a11yProps(2)} />
                    <Tab label="4" {...a11yProps(3)} />
                    <Tab label="5" {...a11yProps(4)} />
                    <Tab label="6" {...a11yProps(5)} />
                    <Tab label="7" {...a11yProps(6)} />
                    <Tab label="8" {...a11yProps(7)} />
                    <Tab label="9" {...a11yProps(8)} />
                    <Tab label="10" {...a11yProps(9)} />
                </Tabs>
            </Box>
            <TabPanel value={value} index={0}>
                <div>
                    <ul className='card-body'>
                        {itemOne}
                    </ul>
                </div>
            </TabPanel>
            <TabPanel value={value} index={1}>
                <div>
                    <ul className='card-body'>
                        {itemsTwo}
                    </ul>
                </div>
            </TabPanel>
            <TabPanel value={value} index={2}>
                <ul className='card-body'>
                    {itemsThree}
                </ul>
            </TabPanel>
            <TabPanel value={value} index={3}>
                <ul className='card-body'>
                    {itemsFour}
                </ul>
            </TabPanel>
            <TabPanel value={value} index={4}>
                <ul className='card-body'>
                    {itemsFive}
                </ul>
            </TabPanel>
            <TabPanel value={value} index={5}>
                <ul className='card-body'>
                    {itemsSix}
                </ul>
            </TabPanel>
            <TabPanel value={value} index={6}>
                <ul className='card-body'>
                    {itemsSeven}
                </ul>
            </TabPanel>
            <TabPanel value={value} index={7}>
                <ul className='card-body'>
                    {itemsEight}
                </ul>
            </TabPanel>
            <TabPanel value={value} index={8}>
                <ul className='card-body'>
                    {itemsNine}
                </ul>
            </TabPanel>
            <TabPanel value={value} index={9}>
                <ul className='card-body'>
                    {itemsTen}
                </ul>
            </TabPanel>
        </Box>
    );
}