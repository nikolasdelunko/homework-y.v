import './Button.css'
import React from 'react';

const Button = (props) => {
    const {classN, text, onClick} = props
    return (
        <div className='btn-container'>
            <button
                className={classN}
                onClick={onClick}
            >{text}</button>
        </div>
    );
};

export default Button;
