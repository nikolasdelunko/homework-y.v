import React from 'react';
import './item.css'
import Button from "../Button/button";
import {connect} from "react-redux";
import {useSelector} from "react-redux";
import Modal from "../modal/Modal";


const Items = (props) => {
    const {card: {albumId, id, title, thumbnailUrl},
        setModal,
        setItem,
        setCurrentId,
        setCurrentUrl,
    } = props


    const items = useSelector(state => state.Products)
    const modal = useSelector(state => state.modal.modal)
    const currentId = useSelector(state => state.modal.currentId)
    const currentUrl = useSelector(state => state.modal.currentUrl)

    const modalClose = () => {
        setModal(false)
    }

    const openModal = (id) => {
        setModal(true)
        const newCard = items.data.filter((card) => card.id === id)
        const [{...addToCard}] = newCard
        setCurrentUrl(addToCard.url)
        setCurrentId(addToCard.id)
    }

    const deleteProduct = (currentId) => {
        setItem([...items.data.filter((products) => products.id !== currentId)])
        modalClose()
        console.log(id)
    }


    return (
        <>
                <li className='card-product'>
                    <div className='card-head'>
                        <div className='card-img'>
                            <img src={thumbnailUrl} alt='small'/>
                        </div>
                        <span className='card-id'>{albumId}</span>
                        <p className='card-text'>{title}</p>
                        <div className='card-control'>
                        <Button
                        text='DELETE'
                        classN='btn first'
                        onClick={() => openModal(id) }
                        />
                        </div>
                    </div>
                </li>
    {modal && (
        <div className='list-items'>
            <Modal
                header={`Do you wont delete this photo?`}
                onClick={modalClose}
                closeButton={true}
                modalClose={modalClose}
                url={currentUrl}
                actions={
                    <>
                        <Button
                            onClick={() => deleteProduct(currentId)}
                            text='DELETE'
                            classN='btn third'
                        />
                        <Button
                            onClick={() => modalClose()}
                            text='CANCEL'
                            classN='btn third'
                        />
                    </>
                }
            />
        </div>
    )}
        </>
    )

};





const mapDispatchToProps = (dispatch) => {
    return {
        setModal: (Modal) => dispatch({type: "SET_MODAL", payload: Modal}),
        setItem: (ItemProduct) => dispatch({type: "SET_PRODUCTS", payload: ItemProduct}),
        setCurrentId: (currentId) => dispatch({type: "SET_CURRENT_ID", payload: currentId}),
        setCurrentUrl: (currentUrl) => dispatch({type: "SET_CURRENT_URL", payload: currentUrl}),
    }
}
export default connect(null, mapDispatchToProps)(Items)





