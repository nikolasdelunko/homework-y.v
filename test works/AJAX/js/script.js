const btn = document.getElementById('button')
const request = new XMLHttpRequest

request.open('GET', 'https://httpbin.org/cache')

request.onreadystatechange = function () {
    if (request.readyState === 4 && request.status === 200) {
        const res = JSON.parse(request.response)
        console.log(res)
    }
    
}

request.send()