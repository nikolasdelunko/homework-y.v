import  express from 'express'

const PORT = 3000
const app = express()

app.get('/', (req,res) => {
    res.send('<h1>Hello Express</h1>')
})


app.listen(PORT, () => {
    console.log(`server start on port ${PORT}`)
})