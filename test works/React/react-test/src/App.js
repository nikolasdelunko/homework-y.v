import logo from './logo.svg';
import './App.css';
import React, {Component} from 'react'
import ErrorBoundary from "./Components/ErrorBoundary/ErrorBoundary";


class Start extends Component {
    state = {
        title: '',
        age: '',
        hobbies: 'psychology',
        name: '',
    }
    incrementTitle = () => {
        this.setState({title: 'Hello Nastya'})
    }

    incrementAge = () => {
         const {age} = this.state
         this.setState({age: 26})
        // this.setState((({age}) => ({age: age + 0.1})))

    }
    incrementName = () => {
        this.setState({name: 'Nastya'})
    }

    render() {
        const {title, age, name} = this.state
        setInterval(this.incrementTitle, 2000)
        setInterval(this.incrementAge, 3000)
        setInterval(this.incrementName, 1000)
        return (
            <ErrorBoundary>
            <div className="App">
                <header title={title} age={age} name={name} className="App-header">
                    <h2>{name}</h2>
                    <img src={logo} className="App-logo" alt="logo"/>
                    <p>{title}</p>
                    <p>{age}</p>
                </header>
            </div>
            </ErrorBoundary>
        )
    }
}

export default Start;
