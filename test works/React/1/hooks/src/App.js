import './App.css';
import React, {useContext, useState, useEffect} from "react";
import {ThemeContext, HooksContext} from './Components/Item'

function App() {
    const Hooks = useContext(HooksContext)
    const Theme = useContext(ThemeContext)
    const [count, setCount] = useState(0)
    const [value, changeValue] = useState('test value')
    const [width, setWidth] = useState(window.innerWidth)
    const handleChangeValue = (e) => {
        changeValue(e.target.value)
    }


    useEffect(()=>{
        const handleWidth = setWidth(window.innerWidth)
        window.addEventListener('resize', handleWidth)
    })

    useEffect(()=>{
        document.title = value
    })

    return (
    <div className={`card ${Theme}`}>
        <item label='Hooks'>
        <h1>{Hooks}</h1>
        <h2>Nastya the best</h2>
        </item>
        <div>
            <p>count is {count}</p>
            <button onClick={() => {
                setCount(count + 1)
            }}>click me</button>
            <p>You type {value}</p>
            <input
                value={value}
                onChange={handleChangeValue}
            />
            <p>You width is {width}</p>

        </div>
    </div>
  );
}

export default App;
