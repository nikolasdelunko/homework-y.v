// let promise = new Promise (function (resolve, reject) {
//     setTimeout(() => resolve('done'),3000)
//     // setTimeout(() => reject(new Error('oops')),3000)
// })

// promise.then(
//     result => alert(result),
//     error => alert(error)
// )

// promise.catch(alert) //только на Error

// promise.finally(() => alert('Promise done'))
// promise.then(result => alert(result))
// console.log(promise)


// new Promise ((resolve, reject) => {
//     throw new Error('Error')
// })
//
// Promise.finally(()=> alert('promise done'))
// Promise.catch(err => alert(err))


function loadScript(src) {
    return new Promise(function (resolve, reject) {
        let script = document.createElement('script')
        script.src = src

        script.onload = () => resolve(String)
        script.onerror = () => reject(new Error(`ERROR LOAD SCRIPT ${script}`))

        document.head.append(script)
    })

}

let promise = loadScript("https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.js")

promise.then(
    script => alert(`${script.src} Load`),
    error => alert(`ERROR ${error.message}`)
)

promise.then(script => alert('ESHE OBRABOTCHIK...'))



const loadImage = url => {
    return new Promise(function (resolve, reject) {
        let request = new XMLHttpRequest()
        request.open('GET',url)

        request.onload = function () {
            if (request.status === 200) {
                resolve (request.response)
            }
            // else if (request.status === 404){
            //     alert('SERVER BLOCK YOU')
            // }
            else{
                reject(Error('Произошла ошыбка'+ request.statusText))
            }
        }
        request.send()
    })
}

const embedImage = url => {
    loadImage(url).then(function (result) {
        const img = new Image()
        let imageURL = window.URL.createObjectURL(result)
        img.src = imageURL
        document.querySelector('body').prepend(img)
    },
    function(err) {
        console.log(err)
    })
}

embedImage()

// embedImage('https://telegra.ph/file/76bb33f2d6d1c74820ef2.png')
//
// const embedImage = url => {
//     loadImage(url).then(function(result) {
//             const img = new Image();
//             let imageURL = window.URL.createObjectURL(result);
//             img.src = imageURL;
//             document.querySelector('body').appendChild(img);
//         },
//         function(err) {
//             console.log(err);
//         });
// }