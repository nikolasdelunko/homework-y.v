
const btnSgnUp = document.getElementById('topLink')
const worning2Sumb = ('<p id="warningText" class="error-text">The name must be more than 2 characters</p>')
const worningReq = ('<p id="warningText" class="error-text">Fill in the field</p>')


const showError = () => {
    const name = document.getElementById('name').value
    const lastname = document.getElementById('lastname').value
    const country = document.getElementById('country').value
    const Phone = document.getElementById('Phone').value
    const PassComf = document.getElementById('ConfirmPass').value
    const Pass = document.getElementById('Pass').value
    if (name.length <= 2) {
        document.getElementById('name').insertAdjacentHTML('afterEnd', worning2Sumb)
    }
    if (lastname.length <= 2) {
        document.getElementById('lastname').insertAdjacentHTML('afterEnd', worning2Sumb)
    }
    if (country.length <= 1 && Phone.length <= 1) {
        document.getElementById('country').insertAdjacentHTML('afterEnd', worningReq)
        document.getElementById('Phone').insertAdjacentHTML('afterEnd', worningReq)
    }
    if (Pass !== PassComf) {
        document.getElementById('ConfirmPass').insertAdjacentHTML('afterend', '<p id="warningText" class="error-text">Password does not match</p>')
    }
    window.setTimeout(()=>{
        const warningText = document.getElementsByClassName('error-text')
        const worningArr = Array.from(warningText)
        worningArr.forEach(elem => elem.remove())
    },4000)
}

btnSgnUp.addEventListener('click',(event) => {
        showError()
        event.preventDefault()
})

document.addEventListener("keydown", function (event) {
if (window.event.keyCode === 67) {
    showError()
}
})





