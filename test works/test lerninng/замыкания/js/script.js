// function calcNumTest(n) {
//     return function () {
//         console.log(100 * n)
//     }
// }
//
// const test = calcNumTest(31)
// test()


// function Createincrementor (n) {
//     return function (num) {
//         return n + num
//     }
// }
//
// const addOne  = Createincrementor(1)
// const addTen = Createincrementor(10)
//
// console.log(addOne(10))
// console.log(addTen(10))

// function urlGen(domian) {
//     return function (url) {
//         return `https://${url}.${domian}`
//     }
// }
//
//
// const comUrl = urlGen('com')
// const uaUrl = urlGen('com.ua')
//
// console.log(comUrl('google'))
// console.log(comUrl('nastya'))
// console.log(uaUrl('google'))

function bind(context, fn) {
    return function (...args) {
        fn.call(context, args)
    }
}

function logPerson() {
    console.log(`name:${this.name}, age: ${this.age}, city: ${this.city}, work: ${this.work}`)
}

const person1 = {
    name: 'Yaroslav',
    age: 28,
    city: 'Kyiv',
    work: 'js'
}

const person2 = {
    name: 'Anastasiya',
    age: 26,
    city: 'NovaOdessa',
    work: 'psychologist',

}

bind(person1, logPerson) ()
bind(person2, logPerson) ()


//  вариант 2 простой но без функции
// let Yaroslav = logPerson.bind(person1);
// let Nastya = logPerson.bind(person2);
// Yaroslav()
// Nastya()


function makeWorker() {
    let name = "Pete";

    return function() {
        alert(name);
    };
}

let name = "John";

// create a function
let work = makeWorker();

// call it
work();

// try {
//     lalala; // ошибка, переменная не определена!
// } catch(err) {
//     alert(err.name); // ReferenceError
//     alert(err.message); // lalala is not defined
//     alert(err.stack); // ReferenceError: lalala is not defined at (...стек вызовов)
//
//     // Можем также просто вывести ошибку целиком
//     // Ошибка приводится к строке вида "name: message"
//     alert(err); // ReferenceError: lalala is not defined
// }

//
// let json = "{ некорректный JSON }";
// try {
//
//     let user = JSON.parse(json); // <-- тут возникает ошибка...
//     alert( user.name ); // не сработает
//
// } catch (e) {
//
//     // ...выполнение прыгает сюда
//     alert( "Извините, в данных ошибка, мы попробуем получить их ещё раз." );
//     alert( e.name );
//     alert( e.message );

// }
let num = +prompt("Введите положительное целое число?", 35)

let diff, result;

function fib(n) {
    if (n < 0 || Math.trunc(n) != n) {
        throw new Error("Должно быть целое неотрицательное число");
    }
    return n <= 1 ? n : fib(n - 1) + fib(n - 2);
}

let start = Date.now();

try {
    result = fib(num);
} catch (e) {
    result = 0;
} finally {
    diff = Date.now() - start;
}

alert(result || "возникла ошибка");

alert( `Выполнение заняло ${diff}ms` );



function gogitester(a, b) {
    return function  () {
         a +=
        n++
        return n
    }
}

f('10')()