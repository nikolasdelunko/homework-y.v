//////////////////функция конструктор ///////////////

let person = {
    name: 'Yaroslav',
    lastname: 'verbytski',
    age: 28,
    logInfo : function (job, phone) {
        console.group(`${this.name} info:`)
        console.log(`name is ${this.name}`)
        console.log(`age is ${this.age}`)
        console.log(`last name is ${this.lastname}`)
        console.log(`Job is ${job}`)
        console.log(`Phone is ${phone}`)
        console.groupEnd()
    }
}
let Nastya = {
    name: 'Anastasiya',
    lastname: 'Lyakh',
    age: 26,
}

const fnNasya =  person.logInfo.bind(Nastya)
fnNasya('babbysiter', `+38(063) 123 45 69`)
person.logInfo.call(Nastya, 'babbysiter', `+38(063) 123 45 69`)
person.logInfo.apply(Nastya, ['babbysiter' , '+38(063) 123 45 69'])


///////////////////////////////

const array = [1, 2, 3, 4, 5, 6, 7, 8, 9]

function MultySuply(arr, n) {
    return arr.map(function (i) {
        return i * n
    })
    
}
console.log(MultySuply(array, 7))


///////////////////////////////

const Manager = function(name, sales) {
    this.name = name;
    this.sales = sales;
    this.sell = function(thing) {
        this.sales += 1;
        return 'Manager ' + this.name + ' sold ' + thing;
    };

    // return {prop: 'Prop of new object'};
};

const john = new Manager('Anastasiya', 10 );
console.log(john);
// console.log(john.constructor);
// console.log(john.constructor.name);
// console.log(john instanceof Manager);
john.sell('babbiys');


///////////// функции  ////////
//1
// time = 11
// const interval = setInterval( function  () {
//     if (time === 1 ) {clearInterval(interval), alert('GO')}
//     else {  console.log(--time)}
// } , '1000')
////////////  експерементальный старт //////////


function start() {
    time = prompt('через сколько секунд ты готов начать?')
    if (isNaN(time)) {
        alert('веди число'), start()
    } else if (time === null) {
        alert('Досвидания')
        clearInterval(interval)
    }else if (time <= 1) {
        alert('веди число от 1 до 100'), start()
    }
}
const interval = setInterval( function  () {
    if (time === 1 ) {clearInterval(interval), alert('GO')}
    else {  console.log(--time)}
} , '1000')

start()


///2 стрелочные

// const time = (a = 0, b = 0) => a + b
// console.log(time(2, 3))
//  function sumAll(...all) {
//      let result = 0
//      for (let num of all) {
//          result += num
//      }
//      return  result
//  }
//
//  const res = sumAll(1, 2, 3, 5, 6)
// console.log(res)


///// массивы ////
//
// const cars = ["bmw", "opel", "reno", "mercedes"]
// cars.unshift("volga")
// cars.push('peugeot')
// // console.log(cars)
//
// const index = cars.indexOf("opel")
// cars[index] = 'maseratti'
// console.log(index)

const people = [
    {name: "Stepka", money: 5000, card: "visa"},
    {name: "Grisha", money: 100, card: ''},
    {name: "Kolyan", money: 500, card: "masterCard"},
    {name: "bill", money: 5000000, card: "amex"}
]

let findedPerson
for (const major of people) {
    if (major.money === 500) {
        findedPerson = major
    }
}
    console.log(findedPerson)
////// резултат такойже как и сверху

// const major = people.find(function (major) {
//     return major.money === 5000
// })
// console.log(major)

///// такой же вариант как и выше только упрощенный
const major = people.find(major => major.money === 5000)
console.log(major)
// let text = "ехал грека через реку, и за руку грека цап"
// let abraCadabra = text.split(' ').reverse().join(' ')
// console.log(abraCadabra)




/////////////   ООП и классовое наследование /////////

class Person {
    constructor(firstName, lastName) {
        this.firstName = firstName
        this.lastName = lastName
    }
    getFullName() {
        return this.firstName + ' ' + this.lastName
    }
}

let lydina = new Person('Stepan', 'Gogovitch')

lydina.getFullName()


class User extends Person  {
    constructor(firstName, lastName, email, password) {
        super(firstName, lastName);
       this.email = email
        this.password = password
    }
    getEmail(){
        return this.email
    }
    getPassword() {
        return this.password
    }
}

function App() {
    let user = new User(
        'Gogi',
        'Gogovitch',
        'gogigogovitch@email.com',
        '000000'
    )
    user.getFullName()
    user.getEmail()
    user.getPassword()
    user.firstName
    user.lastName
    user.email
    user.password
    
}


let arr = [1,2,3]
arr.forEach(e => console.log(e))
arr