function fetchFilms(url) {
    fetch(url).then((res) => {
            return res.json();
    }).then((data) => {
        const characters = getActors(data);
        displayData(data);
        const loadChars = document.querySelectorAll(".chars");
        characters.forEach(displayCharacters(loadChars));
    })

    function getActors(data) {
        return data.map(({characters}) => {
            return characters;
        });
    }
}

function displayData(data) {
    let output = '';
    data.forEach(item => {
        output += `<div class='film-cart'><div class="Flm-id">Film ID: ${item["episodeId"]}</div><h3>${item.name}</h3>
                    <p><strong>Short description:</strong> ${item["openingCrawl"]}</p><h3>Characters:</h3><ul class="chars"></ul></div>`

    })
    document.body.innerHTML = output;
}

function displayCharacters(loadChars) {
    return function (charList, filmId) {
        const fetchChars = charList.map(charURL => {
            return fetch(charURL).then((res) => {
                return res.json();
            })
        });
        Promise.all(fetchChars).then((data) => {
            const charNames = data.map(({name}) => name);
            charNames.forEach(item => {
                const li = document.createElement("li");
                li.innerText = item;
                loadChars[filmId].append(li);
            });
        });
    }
}

fetchFilms('https://ajax.test-danit.com/api/swapi/films');
