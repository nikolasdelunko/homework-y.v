const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

function createTree(data) {
    const listContainer = document.getElementById('root')
    const ul = document.createElement('ul');
    function renderElem(item, index) {
        const {author, name, price} = item
        let li = document.createElement('li')
        if (!author) {
            throw new Error(`no author in book ${index + 1}`)
        } else if (!name) {
            throw new Error(`no name in book ${index + 1}`)
        } else if (!price) {
            throw new Error(`no price in book ${index + 1}`)
        }

        li.innerHTML = `<p>Author:</p> ${author}<p>Name:</p> ${name}<p>Price:</p> ${price}`
        return li
    }

    data.forEach((item, index) => {

        try {
            const listElem = renderElem(item, index)
            ul.append(listElem)
        } catch (Error) {
            console.log(Error)
        }
    });
    return listContainer.append(ul)
}

createTree(books)
//////////


// function createTree(data) {
//     const ul = document.createElement('ul');
//
//     data.forEach(item => {
//         const author = document.createElement('li')
//         const name = document.createElement('li')
//         const price = document.createElement('li')
//         author.textContent = item.author;
//         name.textContent = item.name;
//         price.textContent = item.price;
//         ul.append('book info:',author, name, price)
//         if (!author) {
//             throw new Error(`no author in book ${index + 1}`)
//         } else if (!name) {
//             throw new Error(`no name in book ${index + 1}`)
//         } else if (!price) {
//             throw new Error(`no price in book ${index + 1}`)
//         }
//
//     });
//     return ul;
// }
//
// document.querySelector('#root').append(createTree(books));