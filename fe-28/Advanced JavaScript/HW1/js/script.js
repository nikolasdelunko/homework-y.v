class Employee {
 constructor(name, age, salary) {
     this._name = name
     this._age = age
     this._salary = salary


 }
    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends  Employee  {
    constructor(name, age, salary) {
        super(name, age, salary);
        this._salary = salary;
    }

    get salary() {
        return this._salary*3;
    }

    set salary(value) {
        this._salary = value;
    }
}

const firstEmployee =  new Programmer ('Nastya','26','200000')
const secondEmployee = new Programmer('Yaroslav', '28', '50000')
const engEmployee = new Programmer('Marsik','5','10000')

console.log(firstEmployee)
console.log(engEmployee)
console.log(secondEmployee)