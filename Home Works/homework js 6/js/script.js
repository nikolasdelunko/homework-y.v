
function filterBy(array, dataType) {

    if (Array.isArray(array)) {
            return array.filter(element => typeof element !== dataType)
        }
    return 'Wrong data type!'
}
console.log(filterBy(['day', 25, null, {}, '43', true, NaN,], 'object'))
