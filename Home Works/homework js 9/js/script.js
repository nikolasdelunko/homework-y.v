let tabs= document.querySelectorAll('.tabs-title')
let allTabs = document.querySelectorAll('.tabs-content li')
addTabListener(tabs,allTabs,'tab','text')

function addTabListener(elements, allTabs, datasetTab, datasetText) {

    function activeTab(newActiveElem) {
        let oldActive = document.querySelector('.active')
        if(oldActive) {
            oldActive.classList.remove('active')
            newActiveElem.classList.add('active')
        }
    }

    if(elements.length > 0 && allTabs.length > 0) {
        elements = [...elements]
        allTabs = [...allTabs]

        elements.map(element => element.addEventListener('click', function() {

            activeTab(this)
            allTabs.map(tab => {
                if(tab.dataset[datasetText] === this.dataset[datasetTab]) {
                    tab.style.display = "block"
                } else {
                    tab.style.display = "none"
                }
            })
        }))
    }
}

