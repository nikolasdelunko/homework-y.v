whiteTheme = document.getElementById('styleWhite')
changeThemeBtn = document.getElementById('change')


function changeTheme(){

    if(localStorage.getItem('theme') === 'default'|| localStorage.getItem('theme') === null){
        whiteTheme.href = "css/style.css"
    }
    else if(localStorage.getItem('theme') === 'darkStyle'){
        whiteTheme.href = "css/darkStyle.css"
    }
    changeThemeBtn.addEventListener('click', ()=>{
        if(localStorage.getItem('theme') === 'default' || localStorage.getItem('theme') === null){
            whiteTheme.href = "css/darkStyle.css"
            localStorage.setItem('theme', 'darkStyle')
        }
        else if(localStorage.getItem('theme') === 'darkStyle'){
            whiteTheme.href = "css/style.css"
            localStorage.setItem('theme', 'default')

        }
    })
}

changeTheme()

