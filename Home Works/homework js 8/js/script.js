function actionColor() {
    let getPrice = document.getElementById('inpPrice')
    getPrice.onfocus = function () {
        getPrice.classList.toggle('inp-green')
    }
    getPrice.onblur = function () {
        let priceCheck = document.getElementById('text')
        getPrice.classList.toggle('inp-green')
        let mainBox = document.getElementById('main')
        if (getPrice.value > null) {
            getPrice.classList.add('inp-green')
            if (priceCheck === null) {
                mainBox.insertAdjacentHTML('afterbegin', `<span id='text'>Current price: ${getPrice.value}<button style="border-radius: 5px" id="cancel">X</button></span>`)
            }
            let cancelBtn = document.getElementById('cancel')
            cancelBtn.addEventListener('click', (event) => cancelBtn.parentNode.remove(event))

            function cleaner() {
                let clean = document.getElementById('error')
                if (clean !== null) {
                    clean.parentNode.removeChild(clean)
                    getPrice.classList.remove('inp-red')
                }
            }

            cleaner()
        } else {
            let error = document.getElementById('error')
            if (error === null && getPrice.value !== '') {
                getPrice.classList.add('inp-red')
                mainBox.insertAdjacentHTML('beforeend', `<span id='error'> Please input only numbers </span>`)
            }
        }

    }
}

actionColor()