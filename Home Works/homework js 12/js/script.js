const slides = document.querySelectorAll('#images .image-to-show')
let currentSlide = 0
let slideInterval = setInterval(nextSlide, 3000)

function nextSlide() {
    slides[currentSlide].className = 'slide'
    currentSlide = (currentSlide + 1) % slides.length
    slides[currentSlide].className = 'slide showing'
}

let playing = true
const pauseButton = document.getElementById('pause')
const resumeButton = document.getElementById('resume')

function pauseSlideshow() {
    playing = false
    clearInterval(slideInterval);
}

function playSlideshow() {
    playing = true
    slideInterval = setInterval(nextSlide, 3000);
}

pauseButton.onclick = function () {
    if (playing) {
        pauseSlideshow()
        resumeButton.removeAttribute('disabled')
    }
}
resumeButton.onclick = function () {
    playSlideshow()
    resumeButton.setAttribute('disabled', 'disabled')
}