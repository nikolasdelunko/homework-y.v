$('li').click(function () {
    $('#panel').slideToggle('slow')
})
$('#panel')

$(function () {
    $(".btn-slide").click(function () {
        $("#panel").slideToggle("slow")
        $(this).toggleClass("active")
        return false;
    })
})

$(document).ready(function () {
    $("#menu-list").on("click", "a", function (event) {
        event.preventDefault()
        const id = $(this).attr('href'),
            top = $(id).offset().top
        $('body,html').animate({scrollTop: top}, 1500)
    })
})

$('#button').click(function () {
    $('body,html').animate({scrollTop: 0}, 2700)
    return false;
})

$(document).ready(function () {
    const btn = $('#button')
    $(window).scroll(function () {
        if ($(window).scrollTop() > 800) {
            btn.addClass('show')
        } else {
            btn.removeClass('show')
        }
    })
})