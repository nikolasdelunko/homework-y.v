import "./App.css";
import React, {useEffect, useState} from 'react';
import Routes from "./Components/Routes/Routes";
import Header from "./Pages/Header/Header";
import Footer from './Pages/Footer/Footer'
import axios from "axios";
import Modal from "./Components/Modal/Modal";
import Button from "./Components/Button/button"


const App = () => {
    const [items, setItems] = useState([])
    const [currentId, setCurrentId] = useState([])
    const [currentName, setCurrentName] = useState([])
    const [modal, setModal] = useState(false)
    const [modalCart, setModalCart] = useState(false)
    const [ItemCart, setItemCart] = useState(
        localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : []
    )
    const [ItemFavorites, setItemFavorites] = useState(
        localStorage.getItem("favorites")
            ? JSON.parse(localStorage.getItem("favorites"))
            : []
    );

    useEffect(() => {
        setTimeout(() => {
            axios('/Goods.json')
                .then(res => {
                    setItems(res.data);
                })
        }, 1500)
        if (!localStorage.getItem("cart")) localStorage.setItem("cart", "[]");
        if (!localStorage.getItem("favorites"))
            localStorage.setItem("favorites", "[]");

    })
    const addToCart = (currentId) => {
        if (!ItemCart.find((card) => card.article === currentId)) {
            const newCard = items.filter((card) => card.article === currentId)
            const [{...addToCard}] = newCard
            setItemCart([...ItemCart, addToCard])
            localStorage.setItem(
                "cart",
                JSON.stringify([...ItemCart, addToCard])
            )
            setModal(false)
        }
    }

    const addFavorites = (id) => {
        if (!ItemFavorites.find((card) => card.article === id)) {
            const newCard = items.filter((card) => card.article === id);
            const [{ ...addToFavorite }] = newCard;
            setItemFavorites([...ItemFavorites, addToFavorite]);
            localStorage.setItem(
                "favorites",
                JSON.stringify([...ItemFavorites, addToFavorite])
            );
        }
    };

    const deleteProduct = (currentId) => {
        setItemCart([...ItemCart.filter((card) => card.article !== currentId)])
        localStorage.setItem(
            "cart",
            JSON.stringify([...ItemCart.filter((card) => card.article !== currentId)])
        )
        modalCloseCart()
    }

    const deleteFavorites = (id) => {
        setItemFavorites([
            ...ItemFavorites.filter((card) => card.article !== id)
        ])
    }

    const openModal = (id) => {
        setModal(true)
        const newCard = items.filter((card) => card.article === id)
        const [{...addToCard}] = newCard
        setCurrentId(addToCard.article)
        setCurrentName(addToCard.title)
    }

    const openDeleteModal = (id) => {
        setModalCart(true)
        const newCard = items.filter((card) => card.article === id)
        const [{...addToCard}] = newCard
        setCurrentId(addToCard.article)
    }

    const modalClose = () => {
        setModal(false)
    }
    const modalCloseCart = () => {
        setModalCart(false)
    }


    return (
        <>
            <div>
                <Header ItemCart={ItemCart} ItemFavorites={ItemFavorites}/>
                <Routes
                    items={items}
                    ItemCart={ItemCart}
                    ItemFavorites={ItemFavorites}
                    setItemFavorites={setItemFavorites}
                    onClick={openModal}
                    deleteProduct={openDeleteModal}
                    addToFavorites={addFavorites}
                    deleteFavorites={deleteFavorites}

                />
                {modal && (
                    <div className='list-items'>
                    <Modal
                        header={"Confirm you choice"}
                        modalOne={true}
                        onClick={modalClose}
                        closeButton={true}
                        modalClose={modalClose}
                        text={`Do you wont add ${currentName} to cart?`}
                    actions = {
                        <>
                            <Button
                            onClick={() => addToCart(currentId)}
                            text='Add'
                            classN='modal__control-btn'
                            />
                            <Button
                                onClick={() => modalClose()}
                                text='Cancel'
                                classN='modal__control-btn'
                                />
                        </>
                    }
                    />
                    </div>
                )}
                {modalCart && (
                    <div className='list-items'>
                    <Modal
                        header={"Confirm you choice"}
                        modalOne={true}
                        onClick={modalCloseCart}
                        closeButton={true}
                        modalClose={modalClose}
                        text={`Do you wont remove ${currentName} from  cart?`}
                        actions = {
                            <>
                                <Button
                                    onClick={() => deleteProduct(currentId)}
                                    text='Delete'
                                    classN='modal__control-btn'
                                />
                                <Button
                                    onClick={() => modalCloseCart()}
                                    text='Cancel'
                                    classN='modal__control-btn'
                                />
                            </>
                        }
                    />
                    </div>
                )}
                <Footer/>
            </div>

        </>
    );

}

export default App;


