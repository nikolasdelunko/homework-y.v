import React from 'react';
import './Cart.css'
import Product from "../Products/Products";
import PropTypes from "prop-types";


const Cart = (props) => {
    const { items, deleteProduct } = props;

    const cards = items.map((item) => (
        <Product
            card={item}
            key={item.article}
            deleteProduct={deleteProduct}
            productsCart={items}
            showFavIcon={false}
            showDelBtn={true}
        />
    ));
    return (
        <div>
        <ul className='items-page'>{cards}</ul>
        </div>
    );
};

Cart.propTypes = {
    deleteProduct: PropTypes.func,
    items: PropTypes.array,
};

export default Cart;
