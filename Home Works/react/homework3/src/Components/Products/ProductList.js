import React from "react";
import Products from "./Products";
import Loader from "../Loader/Loader";


const ProductList = (props) => {
    const {
        items,
        ItemFavorites,
        onClick,
        addFavorites,
        deleteFavorites,
        ItemCart,
        setItemFavorites,
    } = props

const cards = items.map((item) => (
    <Products
        items={items}
        card={item}
        key={item.article}
        onClick={onClick}
        addFavorites={addFavorites}
        deleteFavorites={deleteFavorites}
        ItemFavorites={ItemFavorites}
        setItemFavorites={setItemFavorites}
        ItemCart={ItemCart}
        showFavIcon={true}
        showBuyBtn={true}
    />
));

    return (
            <div>
                <ul className='items-page'>{cards}</ul>
                    {cards.length === 0 && <Loader/>}
            </div>)

}

export default ProductList;

