import React from 'react';
import {Redirect, Route} from "react-router-dom";
import Cart from "../Cart/Cart";
import Favorites from "../../Pages/Favorites/Favorites";
import ProductList from "../Products/ProductList";


const Routes = (props) => {
    const {items, ItemCart, ItemFavorites, onClick, deleteProduct, addFavorites, deleteFavorites, setItemFavorites} = props
    return (
        <div className='Main'>
            <div>
                <Redirect exact from='/' to='/shop'/>
                <Route exact path='/shop'
                       render={() => (
                           <ProductList
                               items={items}
                               ItemFavorites={ItemFavorites}
                               setItemFavorites={setItemFavorites}
                               onClick={onClick}
                               addFavorites={addFavorites}
                               deleteFavorites={deleteFavorites}
                               ItemCart={ItemCart}
                           />
                       )}
                />
                <Route exact path='/cart'
                       render={() => (<Cart
                               items={ItemCart}
                               deleteProduct={deleteProduct}
                />
                )}
                    />
                <Route exact path='/favorites'
                render={() => (<Favorites
                        items={ItemFavorites}
                        deleteFavorites={deleteFavorites}
                        onClick={onClick}
                    />
                )}
                />
            </div>
        </div>
    );
};

export default Routes;