import Star from "../Theme/icon/Star";
import PropTypes from "prop-types";
import '../Fovorite/Favorite.css'
import React, {useEffect, useState} from 'react';

const Favorite = (props) => {
    const [isIconActive, setIconActive] = useState(false)
    const {article, items, className, ItemFavorites, setItemFavorites} = props

    useEffect(() => {
        if (ItemFavorites.find((card) => card.article === article)) {
            setIconActive(true);
        }
    }, [ItemFavorites, article]);

    const iconActive = () => {
        if (!isIconActive) {
            addFavorites(article);
            setIconActive(true);

        } else {
            deleteFavorites(article);
            setIconActive(false);
        }
    };

    const addFavorites = (id) => {
        if (!ItemFavorites.find((card) => card.article === id)) {
            const newCard = items.filter((card) => card.article === id)
            const [{...addToFavorite}] = newCard
            setItemFavorites([...ItemFavorites, addToFavorite])
            localStorage.setItem(
                'favorites',
                JSON.stringify([...ItemFavorites, addToFavorite])
            )
        }
    }

    const deleteFavorites = (id) => {
        setItemFavorites([
            ...ItemFavorites.filter((card) => card.article !== id)
        ])
        localStorage.setItem(
            'favorites',
            JSON.stringify([...ItemFavorites.filter((card) => card.article !== article)])
        )
    }

    return (
        <>
            <div className='Star' onClick={iconActive}>
                <Star
                    className={className}
                    filled={isIconActive}
                    type="star"
                />
            </div>
        </>
    );
};

Favorite.propTypes = {
    className: PropTypes.string.isRequired,
    article: PropTypes.number,
    addFavorites: PropTypes.func,
    deleteFavorites: PropTypes.func,
    ItemFavorites: PropTypes.array,
};

export default Favorite;

