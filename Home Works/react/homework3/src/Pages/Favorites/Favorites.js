import React from 'react';
import './Favorites.css'
import Products from "../../Components/Products/Products";
import PropTypes from "prop-types";

const Cart = (props) => {
    const { items, deleteFavorites, onClick} = props;

    const cards = items.map((item) => (
        <Products
            card={item}
            onClick={onClick}
            key={item.article}
            deleteFavorites={deleteFavorites}
            porductsFavorites={items}
            productsCart={items}
            // showFavIcon={true}
            showBuyBtn={true}
        />
    ));
    return <ul className='items-page'>{cards}</ul>;
};

Cart.propTypes = {
    deleteFavorites: PropTypes.func,
    items: PropTypes.array,
    onClick: PropTypes.func,
};

export default Cart;

