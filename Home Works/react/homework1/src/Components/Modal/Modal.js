import './Modal.scss'
import Button from '../Button/button'
import React, {Component} from 'react';

class Modal extends Component {
    closeModalByClickOutside = (e) => {
        if (!e.target.closest(".modal__container")) {
            this.props.closeModal();
        }
    }

    render() {
        const {isOpened, text, header, closeModal, okButton} = this.props
        return (
            <div className={`modal  ${isOpened ? 'open' : 'close'} `} onClick={this.closeModalByClickOutside}>
                <div className='modal__container'>
                    <div className='modal__header'>
                        <span className='modal__header-title'>{header}</span>
                        <span className='modal__header-close' onClick={closeModal}/>
                    </div>
                    <div className='modal__content'>
                        <p className='modal__content-text'>{text}</p>
                    </div>
                    <div className='modal__control'>
                        {/*<button className='modal__control-btn' onClick={okButton}>confirm</button>*/}
                        {/*<button className='modal__control-btn' onClick={closeModal}>Cancel</button>*/}
                        <Button classN='modal__control-btn' openModal={okButton} text='Confirm'/>
                        <Button classN='modal__control-btn' openModal={closeModal} text='Cancel'/>
                    </div>
                </div>
            </div>
        );
    }
}

export default Modal;