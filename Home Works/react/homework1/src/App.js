import React, {Component} from 'react';
import './App.scss';
import Button from './Components/Button/button';
import Modal from "./Components/Modal/Modal";


class App extends Component {
    state = {
        status: '',
        modal1: false,
        modal2: false
    }

    render() {
        const {modal1, modal2} = this.state
        return (

                <div className="App-header">
                        <Button
                            openModal={() => this.setState({modal1: true})}
                            text='Open first modal'
                            classN='btn first'
                        />
                        <Button
                            openModal={() => this.setState({modal2: true})}
                            text='Open second modal'
                            classN='btn third'
                        />

                        <Modal
                            header={"Modal 1 title"}
                            isOpened={modal1}
                            closeModal={() => this.setState({modal1: false})}
                            text='Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus.'
                            okButton={() => alert('Almost Done')}

                        />
                        <Modal
                            header={"Modal 2 title"}
                            isOpened={modal2}
                            closeModal={() => this.setState({modal2: false})}
                            text="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam magni non porro qui quos recusandae."
                            okButton={() => alert('Wait pls')}
                        />
                </div>

        )
    }
}

export default App;



