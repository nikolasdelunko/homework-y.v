import React from "react";
import Product from "./Products";


class ProductList extends React.Component {


    render() {
        const {goods} = this.props;
        const cards = goods.map((item) => (
            <Product card={item} key={item.article}/>
        ));
        return (
            <div>
                <ul className='items-page'>{cards}</ul>
            </div>)
    }
}

export default ProductList;
