import React from "react";
import "./Products.module.scss"
import PropTypes from "prop-types";
import Button from "../Button/button";
import Modal from "../Modal/Modal";
import Favorite from "../Fovorite/Favorite";

class Product extends React.Component {
    state = {
        modal: false,
    }
    targetProd = null;
    favorites = null;


    render() {
        const {modal, filled = false} = this.state
        const {card: {title, price, article, url, color,}} = this.props;

        const toggleAddFav = (art = {article}) => {
            // console.log(`Add to Favorites ${article} ${filled}`)
            let favProds = JSON.parse(localStorage.getItem("favorites"));
            let newFavProds = favProds.map((el) => el);
            newFavProds.push(article);
            localStorage.setItem("favorites", JSON.stringify(newFavProds));
            this.setState({
                ...this.state,
                filled: true,
            });
            this.favorites = article
            if (filled === true){
                this.setState({
                    filled : false,
                    }
                )
            }
        }
        const addToCart = () => {
            let cartProds = JSON.parse(localStorage.getItem("cart"));
            let newProds = cartProds.map((el) => el);
            newProds.push(article);
            localStorage.setItem("cart", JSON.stringify(newProds));
            this.setState({
                ...this.state,
            });
            this.targetProd = article
        };

        return (
            <div className='list-items'>
                <li className='card-product'>
                    <div className='card-head'>
                        <h3>{title}</h3>
                        <Favorite type="star" filled={filled} className='Star' func={toggleAddFav}/>
                    </div>
                    <div>
                        <img src={url} width="200" height="180" alt="laptop"/>
                    </div>
                    <span className='Color'>Color: {color}</span>
                    <span className='Price'>Price: {price} $</span>
                    <span>SKU: {article}</span>
                    <Button
                        openModal={() => this.setState({modal: true})}
                        text='Add to cart'
                        classN='btn first'
                    />
                </li>
                <Modal
                    header={"Confirm you choice"}
                    isOpened={modal}
                    closeModal={() => this.setState({modal: false})}
                    text={`Do you wont add ${title} to cart?`}
                    okButton={() => {addToCart();  this.targetProd = article; this.setState({modal: false})}}
                />
            </div>
        );
    }
}

Product.propTypes = {
    card: PropTypes.shape({
        title: PropTypes.string,
        article: PropTypes.number,
        price: PropTypes.number,
        url: PropTypes.string,
        color: PropTypes.string,
    }),
};

export default Product;
