import './Button.scss'
import React, {Component} from 'react';



class Button extends Component {

    render() {
        const {openModal, classN, text} = this.props
        return (
            <div className='btn-container'>
                <button className={classN} onClick={openModal}>{text}</button>
            </div>
        );
    }
}

export default Button;
