import React, {Component} from 'react';
import Star from "../Theme/icon/Star";
import PropTypes from "prop-types";
import '../Fovorite/Favorite.css'

class Favorite extends Component {

    render() {
        const {filled = false, func,  className} = this.props;
        return (
            <div>
                <div className="Star" onClick={func}>
                    <Star className={className} type="star" filled={filled} />
                </div>

            </div>
        );
    }
}

Favorite.propTypes = {
    className: PropTypes.string.isRequired,
    func: PropTypes.func,
    id: PropTypes.string,
};
Favorite.defaultProps = {
    func: null,
};



export default Favorite;