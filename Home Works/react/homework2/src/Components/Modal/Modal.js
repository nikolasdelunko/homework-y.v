import './Modal.scss'

import React, {Component} from 'react';
import Button from "../Button/button";

class Modal extends Component {
    closeModalByClickOutside = (e) => {
        if (!e.target.closest(".modal__container")) {
            this.props.closeModal();
        }
    }
    render() {
        const {isOpened, text, header, closeModal, okButton} = this.props
        return (
            <div className={`modal  ${isOpened ? 'open' : 'close'} `} onClick={this.closeModalByClickOutside}>
                <div className='modal__container'>
                    <div className='modal__header'>
                        <span className='modal__header-title'>{header}</span>
                       <span className='modal__header-close' onClick={closeModal} />
                    </div>
                    <div className='modal__content'>
                        <p className='modal__content-text'>{text}</p>
                    </div>
                    <div className='modal__control'>
                        <Button classN='modal__control-btn' openModal={okButton} text='Confirm'/>
                        <Button classN='modal__control-btn' openModal={closeModal} text='Cancel'/>
                    </div>
                </div>
            </div>
        );
    }
}

export default Modal;