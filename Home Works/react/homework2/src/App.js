import "./App.css";
import axios from "axios";
import React, {Component} from 'react';
import Loader from "./Components/Loader/Loader";
import ProductList from "./Components/Products/ProductList";



class App extends Component {
    state = {
        goods: []
    }
    targetProd = null;


    setGoods = (goodsAdd) => {
        this.setState({
            goods: goodsAdd,
        });
    };
    componentDidMount () {
        setTimeout(() => {
            axios('/Goods.json')
                .then(res => {
                    this.setGoods(res.data);
                })
        }, 1500)
        if (!localStorage.getItem("cart")) localStorage.setItem("cart", "[]");
        if (!localStorage.getItem("favorites"))
            localStorage.setItem("favorites", "[]");

    }
    render() {
        const {goods} = this.state
        return (
            <>
            <div className='Main'>
                <ProductList goods={goods} />
            </div>
        {goods.length === 0 && <Loader/>}
            </>
        );
    }
}

export default App;
