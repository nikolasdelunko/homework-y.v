import {combineReducers} from "redux";
import ItemCart from './cart'
import ItemFavorites from './favorites'
import Products from './products'
import modal from './modal'



const reducer = combineReducers({
    ItemCart,
    ItemFavorites,
    Products,
    modal,
})


export default reducer
