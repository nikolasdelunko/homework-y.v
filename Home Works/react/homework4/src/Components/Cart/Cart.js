import React from 'react';
import './Cart.css'
import Product from "../Products/Products";
import PropTypes from "prop-types";
import {useSelector} from "react-redux";


const Cart = (props) => {
    const {deleteProduct} = props;

    const ItemCart = useSelector(state => state.ItemCart.data)

    const cards = ItemCart.map((item) => (
        <Product
            card={item}
            key={item.article}
            deleteProduct={deleteProduct}
            productsCart={ItemCart}
            showFavIcon={false}
            showDelBtn={true}
        />
    ));
    return (
        <div>
        <ul className='items-page'>{cards}</ul>
        </div>
    );
};

Cart.propTypes = {
    deleteProduct: PropTypes.func,
    items: PropTypes.array,
};

export default Cart;
