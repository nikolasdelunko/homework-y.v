showPass = function () {
    const seePass = document.body.querySelector('form')
    seePass.addEventListener('click', (event) => {
        if (event.target.classList.contains('icon-password')) {
            if (event.target.classList.contains('fa-eye')) {
                event.target.className = event.target.className.replace('fa-eye', 'fa-eye-slash')
                event.target.parentElement.querySelector('input').type = 'text'
            } else {
                event.target.className = event.target.className.replace('fa-eye-slash', 'fa-eye')
                event.target.parentElement.querySelector('input').type = 'password'
            }

        }
    })
}

showPass()

function submitCheck() {
    const submit = document.querySelector('button')
    submit.addEventListener('click', () => {
        let inp1 = document.getElementById('inp1')
        let inp2 = document.getElementById('inp2')
        if (inp1.value !== inp2.value) {
            document.body.insertAdjacentHTML('afterend', '<p class="error" style="color: #ff0000">Нужно ввести одинаковые значения</p>')
        }
        else if (inp1.value === '' && inp2.value === '') {
            alert('Нужно что то ввести')
        } else
            alert('You are welcome')


    })
}
submitCheck()



