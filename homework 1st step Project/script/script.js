let tabs = [
    {
        category: 'web design',
        image: 'img/services/service_tab.png',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellat fuga maxime dignissimos deleniti dolore voluptatem, veritatis facere quae esse harum saepe ex corrupti rem laboriosam quisquam labore nam nesciunt. At, optio natus? Exercitationem dicta vero deleniti hic voluptate debitis pariatur minus ducimus distinctio eum impedit, possimus obcaecati maxime velit blanditiis!'
    },
    {
        category: 'graphic design',
        image: 'img/services/wordpress2.jpg',
        description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Explicabo quo consequatur ea doloremque corrupti architecto aut aspernatur, autem fugit temporibus!'
    },
    {
        category: 'online suppoort',
        image: 'img/services/wordpress5.jpg',
        description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Explicabo quo consequatur ea doloremque corrupti architecto aut aspernatur, autem fugit temporibus!'
    },
    {
        category: 'app design',
        image: 'img/services/wordpress9.jpg',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto officiis impedit repellat maiores repellendus delectus distinctio dicta dolorum fugit mollitia quo doloribus commodi, corrupti earum recusandae error veritatis possimus officia iure inventore. Neque magni officia excepturi id qui magnam ullam quae, similique quidem quibusdam autem accusantium cumque fugiat cupiditate maiores veniam optio consequatur. Sit id repellendus, ex delectus consequuntur natus et asperiores suscipit, eligendi esse est iste illo vel quae assumenda blanditiis? Eos eligendi et sint laboriosam voluptatum autem, debitis beatae, nemo maiores veniam accusantium dicta architecto ut est obcaecati tempore doloremque. Sapiente at minus suscipit fugiat assumenda placeat molestiae ullam numquam nam, beatae ratione aut distinctio voluptatibus eveniet temporibus voluptas quia nihil explicabo ab asperiores? Quo quis libero consectetur velit eos, vitae nesciunt molestias quia officiis dolorem perspiciatis repellendus dicta sit possimus deserunt sapiente numquam qui quae, quidem aliquam illum commodi sed unde? Eaque beatae laboriosam iste nam repudiandae.'
    },
    {
        category: 'online marketing',
        image: 'img/services/web-design3.jpg',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates magni ducimus placeat deleniti obcaecati possimus, quaerat alias nesciunt culpa quas hic aut distinctio quis consequatur ut repudiandae veritatis eligendi, cupiditate eaque earum. Ullam tempore ducimus aliquam minus pariatur autem reprehenderit.'
    },
    {
        category: 'seo service',
        image: 'img/services/landing-page1.jpg',
        description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nulla, modi aut exercitationem sequi nesciunt nobis praesentium ipsa, tenetur debitis voluptatibus distinctio repellat, velit vel corporis deleniti soluta fugiat saepe dolorem neque. Vel repellendus vero doloribus iure consectetur magni tenetur officia molestiae molestias, culpa error cumque! Quibusdam praesentium dolorum tempore suscipit quidem rerum culpa porro distinctio nobis nam. Doloremque, repellat veniam?'
    }
]


outputTabs(tabs)

function outputTabs(tabArr) {
    if (tabArr.length > 0) {
        let serviceTabsBlock = document.querySelector('.theme-services-tabs')

        serviceTabsBlock.insertAdjacentHTML('afterbegin', tabArr.map(elem => {
            return `
            <li class="tab-block" data-tab-block="${elem.category.split(' ').join('_')}">
                <img class="tab-image" src="${elem.image}" alt="${elem.category}">
                <p class="tab-text">${elem.description}</p>
            </li>
            `
        }).join(''))
    }
}


let serviceRadios = document.querySelectorAll('.tab-button'),
    serviceTabs = document.querySelectorAll('.tab-block')
if (serviceRadios && serviceTabs) {
    serviceRadios.forEach(button => {

        button.onclick = function () {
            if (button.checked) {
                serviceTabs.forEach(block => {
                    if (block.dataset.tabBlock && block.dataset.tabBlock == this.id.replace('service_', '')) {
                        block.style.display = 'flex'
                    } else {
                        block.style.display = 'none'
                    }
                })
            }
        }
    })
}


let amaizingWorksArray = [
    {
        category: 'graphic design',
        image: 'img/amazing_works/graphic-design5.jpg',
        title: 'creative design',
    },
    {
        category: 'graphic design',
        image: 'img/amazing_works/Layer 24.png',
        title: 'creative design',
    },
    {
        category: 'web design',
        image: 'img/amazing_works/Layer 25.png',
        title: 'creative design',
    },
    {
        category: 'web design',
        image: 'img/amazing_works/Layer 26.png',
        title: 'creative design',
    },
    {
        category: 'web design',
        image: 'img/amazing_works/Layer 27.png',
        title: 'creative design',
    },
    {
        category: 'web design',
        image: 'img/amazing_works/Layer 28.png',
        title: 'creative design',
    },
    {
        category: 'landing pages',
        image: 'img/amazing_works/Layer 29.png',
        title: 'landing design',
    },
    {
        category: 'web design',
        image: 'img/amazing_works/Layer 30.png',
        title: 'creative design',
    },
    {
        category: 'wordpress',
        image: 'img/amazing_works/Layer 31.png',
        title: 'creative wordpress',
    },
    {
        category: 'landing pages',
        image: 'img/amazing_works/Layer 32.png',
        title: 'landing design',
    },
    {
        category: 'wordpress',
        image: 'img/amazing_works/Layer 33.png',
        title: 'creative wordpress',
    },
    {
        category: 'landing pages',
        image: 'img/amazing_works/Layer 34.png',
        title: 'landing design',
    },

    {
        category: 'graphic design',
        image: 'img/amazing_works/graphic-design5.jpg',
        title: 'creative design',
    },
    {
        category: 'graphic design',
        image: 'img/amazing_works/Layer 24.png',
        title: 'creative design',
    },
    {
        category: 'web design',
        image: 'img/amazing_works/Layer 25.png',
        title: 'creative design',
    },
    {
        category: 'web design',
        image: 'img/amazing_works/Layer 26.png',
        title: 'creative design',
    },
    {
        category: 'web design',
        image: 'img/amazing_works/Layer 27.png',
        title: 'creative design',
    },
    {
        category: 'web design',
        image: 'img/amazing_works/Layer 28.png',
        title: 'creative design',
    },
    {
        category: 'landing pages',
        image: 'img/amazing_works/Layer 29.png',
        title: 'landing design',
    },
    {
        category: 'web design',
        image: 'img/amazing_works/Layer 30.png',
        title: 'creative design',
    },
    {
        category: 'wordpress',
        image: 'img/amazing_works/Layer 31.png',
        title: 'creative wordpress',
    },
    {
        category: 'landing pages',
        image: 'img/amazing_works/Layer 32.png',
        title: 'landing design',
    },
    {
        category: 'wordpress',
        image: 'img/amazing_works/Layer 33.png',
        title: 'creative wordpress',
    },
    {
        category: 'landing pages',
        image: 'img/amazing_works/Layer 34.png',
        title: 'landing design',
    },

    {
        category: 'graphic design',
        image: 'img/amazing_works/graphic-design5.jpg',
        title: 'creative design',
    },
    {
        category: 'graphic design',
        image: 'img/amazing_works/Layer 24.png',
        title: 'creative design',
    },
    {
        category: 'web design',
        image: 'img/amazing_works/Layer 25.png',
        title: 'creative design',
    },
    {
        category: 'web design',
        image: 'img/amazing_works/Layer 26.png',
        title: 'creative design',
    },
    {
        category: 'web design',
        image: 'img/amazing_works/Layer 27.png',
        title: 'creative design',
    },
    {
        category: 'web design',
        image: 'img/amazing_works/Layer 28.png',
        title: 'creative design',
    },
    {
        category: 'landing pages',
        image: 'img/amazing_works/Layer 29.png',
        title: 'landing design',
    },
    {
        category: 'web design',
        image: 'img/amazing_works/Layer 30.png',
        title: 'creative design',
    },
    {
        category: 'wordpress',
        image: 'img/amazing_works/Layer 31.png',
        title: 'creative wordpress',
    },
    {
        category: 'landing pages',
        image: 'img/amazing_works/Layer 32.png',
        title: 'landing design',
    },
    {
        category: 'wordpress',
        image: 'img/amazing_works/Layer 33.png',
        title: 'creative wordpress',
    },
    {
        category: 'landing pages',
        image: 'img/amazing_works/Layer 34.png',
        title: 'landing design',
    },

    {
        category: 'graphic design',
        image: 'img/amazing_works/graphic-design5.jpg',
        title: 'creative design',
    },
    {
        category: 'graphic design',
        image: 'img/amazing_works/Layer 24.png',
        title: 'creative design',
    },
    {
        category: 'web design',
        image: 'img/amazing_works/Layer 25.png',
        title: 'creative design',
    },
    {
        category: 'web design',
        image: 'img/amazing_works/Layer 26.png',
        title: 'creative design',
    },
    {
        category: 'web design',
        image: 'img/amazing_works/Layer 27.png',
        title: 'creative design',
    },
    {
        category: 'web design',
        image: 'img/amazing_works/Layer 28.png',
        title: 'creative design',
    },
    {
        category: 'landing pages',
        image: 'img/amazing_works/Layer 29.png',
        title: 'landing design',
    },
    {
        category: 'web design',
        image: 'img/amazing_works/Layer 30.png',
        title: 'creative design',
    },
    {
        category: 'wordpress',
        image: 'img/amazing_works/Layer 31.png',
        title: 'creative wordpress',
    },
    {
        category: 'landing pages',
        image: 'img/amazing_works/Layer 32.png',
        title: 'landing design',
    },
    {
        category: 'wordpress',
        image: 'img/amazing_works/Layer 33.png',
        title: 'creative wordpress',
    },
    {
        category: 'landing pages',
        image: 'img/amazing_works/Layer 34.png',
        title: 'landing design',
    },
]

let number = 12
outputCategories(amaizingWorksArray, number)

function outputCategories(worksArr, number, start = 0) {
    if (worksArr.length > 0) {
        let categoriesBlock = document.querySelector('.filter-block'),
            categoriesContent = ``

        for (let i = start; i < number; i++) {
            if (worksArr[i]) {

                categoriesContent += `
                <li class="filter-block-elem" data-category="${worksArr[i].category.split(' ').join('_')}">
                    <img class="filter-block-elem-image" src="${worksArr[i].image}" alt="${worksArr[i].title}">
                    <div class="filter-block-elem-info">
                        <div class="elem-info-links">
                            <button class="elem-info-link">
                                <svg width="15" height="15" viewBox="0 0 15 15">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#fff">
                                </svg>
                            </button>
                            <button class="elem-info-link">
                                <svg width="12" height="12" viewBox="0 0 60 60">
                                    <rect width="60" height="60" fill="#fff">
                                </svg>
                            </button>
                        </div>
                        <h6 class="elem-info-title">${worksArr[i].title}</h6>
                        <p class="elem-info-category">${worksArr[i].category}</h6>
                    </div>
                </li>
                `
            }
        }
        categoriesBlock.insertAdjacentHTML('beforeend', categoriesContent)
    }
}


let moreBtn = document.getElementById('more_works')
moreBtn.onclick = function () {
    let worksInBlock = document.querySelectorAll('.filter-block-elem')

    if (worksInBlock.length > 0) {

        preloaderToggle()
        setTimeout(() => {
            if (worksInBlock.length < 36) {
                outputCategories(amaizingWorksArray, number)
            }


            if (document.querySelectorAll('.filter-block-elem').length >= 36) {
                this.style.visibility = 'hidden'
            }
        }, 2700)

        preloaderToggle()
    }
}


let filterButtons = document.querySelectorAll('.filter-button')

filterButtons.forEach(btn => {
    btn.onclick = function () {
        let worksInBlock = document.querySelectorAll('.filter-block-elem'),
            filterName = this.id.replace('filter_', '')

        if (worksInBlock.length > 0) {
            worksInBlock.forEach(work => {
                if (filterName === 'all') {
                    work.style.display = 'inline-block'
                } else {
                    (filterName === work.dataset.category)
                        ? work.style.display = 'inline-block'
                        : work.style.display = 'none'
                }
            })
        }
    }
})


let breakingNewsArray = [
    {
        link: '#',
        date: [12, 'feb'],
        image: 'img/breaking_news/Layer_47.png',
        title: 'amazing blog post',
        credentials: ['by admin', '2 comments']
    },
    {
        link: '#',
        date: [12, 'apr'],
        image: 'img/breaking_news/Layer_50.png',
        title: 'amazing blog post',
        credentials: ['by admin', '2 comments']
    },
    {
        link: '#',
        date: [12, 'jul'],
        image: 'img/breaking_news/Layer_51.png',
        title: 'amazing blog post',
        credentials: ['by admin', '2 comments']
    },
    {
        link: '#',
        date: [12, 'oct'],
        image: 'img/breaking_news/Layer_52.png',
        title: 'amazing blog post',
        credentials: ['by admin', '2 comments']
    },
    {
        link: '#',
        date: [1, 'aug'],
        image: 'img/breaking_news/Layer_53.png',
        title: 'amazing blog post',
        credentials: ['by admin', '2 comments']
    },
    {
        link: '#',
        date: [5, 'sep'],
        image: 'img/breaking_news/Layer_54.png',
        title: 'amazing blog post',
        credentials: ['by admin', '2 comments']
    },
    {
        link: '#',
        date: [12, 'oct'],
        image: 'img/breaking_news/Layer_55.png',
        title: 'amazing blog post',
        credentials: ['by admin', '2 comments']
    },
    {
        link: '#',
        date: [12, 'nov'],
        image: 'img/breaking_news/Layer_56.png',
        title: 'amazing blog post',
        credentials: ['by admin', '2 comments']
    },
]

outputBreakingNews(breakingNewsArray)

function outputBreakingNews(newsArr) {
    if (newsArr.length > 0) {
        let breakingNewsBlock = document.querySelector('.breaking-news-posts')

        if (newsArr) {
            breakingNewsBlock.insertAdjacentHTML('afterbegin', newsArr.map(elem => {

                let credentials = ``
                if (elem.credentials.length > 0) {
                    credentials = elem.credentials.map(cred => `<p class="post-credentials-item">${cred[0].toUpperCase() + cred.slice(1)}</p>`).join(' ')
                }

                return `
                <li class="breaking-news-elem">
                    <a class="breaking-news-link" href="${elem.link}">
                        <img src="${elem.image}" alt="${elem.title}" class="post-image">
                        <h6 class="post-title">${elem.title}</h6>
                    
                        ${credentials ? `<div class="post-credentials">${credentials}</div>` : ''} 
                    
                        ${(elem.date.length > 0) ? `<div class="post-date"><p class="post-date-date"> ${elem.date[0]} </p> <p class="post-date-month"> ${elem.date[1]} </p></div>` : ''}  
                    </a>
                </li>
                `
            }).join(''))

        }

    }
}

new Swiper('.image-slider', {
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    },
    pagination: {

        el: '.swiper-pagination',
        clickable: true,
        dynamicBullets: true,
        renderBullet: function (index, className) {
            let imgLink = document.getElementsByClassName('author-photo')


            return '<img class="' + className + '" src="' + imgLink[index].src + '" alt="">'

        }
    },
})


function preloaderToggle() {
    let preloaderBlock = document.querySelector('.preloader-bg')

    if (preloaderBlock.style.display === 'none' || preloaderBlock.style.display === '') {
        preloaderBlock.style.display = 'flex'
    } else {
        setTimeout(function () {
            preloaderBlock.style.display = 'none'
        }, 2700)

    }

}



